@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')
    <style>
        div{
            margin: 10px 0;
            font-family: Lato,sans-serif !important;
            font-size: 16px !important;
        }

    </style>
    <div class="eltd-content" style="margin-top: -90px">
        @foreach($listsafari as $data)
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important; background-image:url('SafariExperience/{{$data->safariexperienceimage}}');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="SafariExperience/{{$data->safariexperienceimage}}" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">{{$data->safariexperiencecaption}}</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper eltd-content-aligment-center">
                            <div class="eltd-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1519737413376">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 ">
                                                    <div class="eltd-eh-item" data-item-class="eltd-eh-custom-8750" data-1280-1600="65px 156px 81px" data-1024-1280="65px 156px 81px" data-768-1024="65px 0 81px" data-680-768="65px 0 81px" data-680="65px 0 81px">
                                                        <div class="eltd-eh-item-inner">
                                                            <div class="eltd-eh-item-content eltd-eh-custom-8750" style="padding: 65px 252px 81px">
                                                                <div class="eltd-section-title-holder   eltd-animated-separator">
                                                                    <div class="eltd-st-inner">
                                                                        <h2 class="eltd-st-title">
                                                                            {{$data->safariexperiencecaption}}</h2>
                                                                        <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                            <div class="eltd-separator"></div>
                                                                        </div>
                                                                        <?php
                                                                        $text = $data->safariexperiencemore;
                                                                        echo $text;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltd-tours-destination-grid">
                                                    <div class="eltd-tours-destination-holder eltd-tours-row eltd-tours-columns-4 eltd-small-space">
                                                        <div class="eltd-tours-row-inner-holder eltd-outer-space">
                                                            @foreach($listdestination as $destination)
                                                                <div class="eltd-tours-destination-grid-item eltd-tours-row-item eltd-item-space post-230 destinations type-destinations status-publish has-post-thumbnail hentry">
                                                                    <div class="eltd-tours-destination-item-holder">
                                                                        <a href="{{ route('ActivityMore',['id'=> $destination->id])}}">
                                                                            <div class="eltd-tours-destination-item-image">
                                                                                <img width="800" height="790" src="ActivityCoverImage/{{$destination->activityimage}}" class="attachment-full size-full wp-post-image" alt="a" srcset="ActivityCoverImage/{{$destination->activityimage}}" sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </div>
                                                                            <div class="eltd-tours-destination-item-content">
                                                                                <div class="eltd-tours-destination-item-content-inner">
                                                                                    <h4 class="eltd-tours-destination-item-title">{{$destination->activitytitle}}</h4>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
            @endforeach
    </div>
    @include('layouts.footer')
@endsection