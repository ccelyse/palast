@extends('layouts.master')
@section('title', 'Palast Tours and Travel')
@section('content')
    @include('layouts.topmenu')
    <style>
        div,span{
            margin: 10px 0;
            font-family: Lato,sans-serif !important;
            font-size: 16px !important;
            line-height: 1.5 !important;
        }
        .eltd-btn.eltd-btn-medium {
            margin-top: 25px;
        }
    </style>
    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            @foreach($listmore as $data)
                <div class="eltd-title-holder eltd-standard-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important;background-image:url('attractions/{{$data->attraction_image}}');" data-height="520">
                    <div class="eltd-title-image">
                        <img itemprop="image" src="attractions/{{$data->attraction_image}}" alt="a" />
                    </div>
                    <div class="eltd-title-wrapper" style="height: 515px">
                        <div class="eltd-title-inner">
                            <div class="eltd-grid">
                                <h1 class="eltd-page-title entry-title" style="color: #ffffff;font-size: 20px !important;">{{$data->attraction_province}}</h1>
                                <h1 class="eltd-page-subtitle" style="color: #ffffff; font-size: 45px !important;">{{$data->attraction_name}}</h1>
                                {{--<a href="{{ route('Enquire',['id'=> $data->id])}}" class="eltd-btn eltd-btn-medium eltd-btn-solid">ENQUIRE</a>--}}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="eltd-container eltd-default-page-template">
            <div class="eltd-container-inner clearfix">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1519120066852" >
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element " >
                                            <div class="wpb_wrapper">
                                                <h2>{{$data->attraction_name}}</h2>
                                            </div>
                                        </div>
                                        <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >
                                            <div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-3738" data-1280-1600="0 7% 0 0" data-1024-1280="0 7% 0 0">
                                                <div class="eltd-eh-item-inner">
                                                    <div class="eltd-eh-item-content eltd-eh-custom-3738" style="padding: 0 42px 0 0">
                                                        <div class="vc_empty_space"   style="height: 13px" ><span class="vc_empty_space_inner"></span></div>
                                                        <div class="wpb_text_column wpb_content_element " >
                                                            <div class="wpb_wrapper">
                                                                <?php
                                                                $str = $data->attraction_indetails;
                                                                ?>
                                                                <div><?php echo $str;?></div>
                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space"   style="height: 43px" ><span class="vc_empty_space_inner"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space"   style="height: 39px" ><span class="vc_empty_space_inner"></span></div>
                                        <div class="vc_empty_space"   style="height: 14px" ><span class="vc_empty_space_inner"></span></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="eltd-sidebar-holder eltd-grid-col-3">--}}
                        {{--<aside class="eltd-sidebar">--}}
                            {{--<div class="widget eltd-tour-list-widget">--}}
                                {{--<div class="eltd-tours-filter-holder eltd-tours-filter-vertical eltd-tours-filter-skin-grey eltd-tours-filter-semitransparent">--}}
                                    {{--<div class="eltd-tours-search-main-filters-holder eltd-boxed-widget">--}}
                                        {{--<form action="#" method="GET">--}}
                                            {{--<div class="eltd-tours-search-main-filters-title">--}}
                                                {{--<h4>Book Perfect Tour</h4>--}}
                                            {{--</div>--}}
                                            {{--<input type="hidden" name="order_by" value="date">--}}
                                            {{--<input type="hidden" name="order_type" value="desc">--}}
                                            {{--<input type="hidden" name="view_type" value="list">--}}
                                            {{--<input type="hidden" name="page" value="1">--}}
                                            {{--<div class="eltd-tours-search-main-filters-fields">--}}
                                                {{--<div class="eltd-tours-input-with-icon">--}}
                                                    {{--<input type="text" value="" class="eltd-tours-destination-search tt-input" name="destination" placeholder="Destination">--}}
                                                    {{--<span class="eltd-tours-input-icon">--}}
                                    {{--<span class="linea-icon linea-basic icon-basic-geolocalize-01"></span>--}}
                                    {{--</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="eltd-tours-input-with-icon">--}}
                                                    {{--<select name="type" class="eltd-tours-select-placeholder">--}}
                                                        {{--<option value="">Tour Type</option>--}}
                                                        {{--<option value="popular">Popular</option>--}}
                                                        {{--<option value="holidays">Holidays</option>--}}
                                                        {{--<option value="on-budget">On budget</option>--}}
                                                        {{--<option value="vacation">Vacation</option>--}}
                                                        {{--<option value="hotel">Hotel</option>--}}
                                                        {{--<option value="tour-guide">Tour guide</option>--}}
                                                        {{--<option value="adventure">Adventure</option>--}}
                                                        {{--<option value="recommended">Recommended</option>--}}
                                                        {{--<option value="explore">Explore</option>--}}
                                                        {{--<option value="tourism">Tourism</option>--}}
                                                        {{--<option value="romantic">Romantic</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                                {{--<div class="eltd-tours-input-with-icon">--}}
                                                    {{--<select name="month" class="eltd-tours-select-placeholder">--}}
                                                        {{--<option  value="">Month</option>--}}
                                                        {{--<option  value="1">January</option>--}}
                                                        {{--<option  value="2">February</option>--}}
                                                        {{--<option  value="3">March</option>--}}
                                                        {{--<option  value="4">April</option>--}}
                                                        {{--<option  value="5">May</option>--}}
                                                        {{--<option  value="6">June</option>--}}
                                                        {{--<option  value="7">July</option>--}}
                                                        {{--<option  value="8">August</option>--}}
                                                        {{--<option  value="9">September</option>--}}
                                                        {{--<option  value="10">October</option>--}}
                                                        {{--<option  value="11">November</option>--}}
                                                        {{--<option  value="12">December</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                                {{--<div class="eltd-tours-input-with-icon">--}}
                                                    {{--<span class="eltd-tours-input-with-icon-label">Price</span>--}}
                                                    {{--<input type="text" id="eltd-tours-price-range-field" class="eltd-tours-price-range-field"--}}
                                                           {{--data-currency-symbol-position="left"--}}
                                                           {{--data-currency-symbol="$"--}}
                                                           {{--data-min-price="1240"--}}
                                                           {{--data-max-price="2390"--}}
                                                           {{--data-chosen-min-price="1240"--}}
                                                           {{--data-chosen-max-price="2390"--}}
                                                           {{--placeholder="Price Range">--}}
                                                    {{--<input type="hidden" name="min_price">--}}
                                                    {{--<input type="hidden" name="max_price">--}}
                                                {{--</div>--}}
                                                {{--<div class="eltd-tours-range-input"></div>--}}
                                                {{--<input type="submit" name="eltd_tours_search_submit" value="Search"  class="eltd-btn eltd-btn-medium eltd-btn-solid"  data-searching-label="Searching..." />--}}
                                            {{--</div>--}}
                                        {{--</form>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</aside>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @include('layouts.footer')
@endsection