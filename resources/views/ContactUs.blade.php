@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')

    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important; background-image:url('frontend/assets/images/34546276580_781daa6846_k.jpg');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="frontend/assets/images/safariexperience.jpg" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">Contact Us</h1>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper eltd-content-aligment-center">
                            <div class="eltd-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1522139090006 vc_row-has-fill" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >
                                                    <div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-9722" data-1280-1600="0 22.5%" data-1024-1280="0 19.5%" data-768-1024="0 12.5%" data-680-768="0 12.5%" data-680="0% 10%">
                                                        <div class="eltd-eh-item-inner">
                                                            <div class="eltd-eh-item-content eltd-eh-custom-9722" style="padding: 0 27.5%">
                                                                <div class="eltd-section-title-holder   text-align-center eltd-animated-separator" style="text-align: center">
                                                                    <div class="eltd-st-inner">
                                                                        {{--<h2 class="eltd-st-title" style="color: #000000">--}}
                                                                            {{--Ask us anything--}}
                                                                        {{--</h2>--}}
                                                                        <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                            <div class="eltd-separator" style="border-color: #000000"></div>
                                                                        </div>
                                                                        <h5 class="eltd-st-text" style="color: #565656">
                                                                            {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. --}}
                                                                        </h5>
                                                                        <p class="eltd-st-text">
                                                                            <a class="eltd-icon-widget-holder"  href="#" target="_self" style="margin: 0 24px 0 8px">
                                                                                <span class="eltd-icon-element fa fa-phone"></span>			<span class="eltd-icon-text ">+250 78831 6763</span>		</a>
                                                                        </p>
                                                                        <p class="eltd-st-text">
                                                                            <a class="eltd-icon-widget-holder"  href="mailto:info@bonvoyage.com" target="_self" style="margin: 0 20px 0 0">
                                                                                <span class="eltd-icon-element fa fa-envelope"></span>			<span class="eltd-icon-text ">info@palasttours.rw</span>		</a>
                                                                        </p>

                                                                    </div>
                                                                </div>
                                                                <div class="vc_empty_space"   style="height: 61px" ><span class="vc_empty_space_inner"></span></div>


                                                                    <form action="{{url('SendingEmail')}}" method="post" class="wpcf7-form cf7_custom_style_1" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="eltd-contact-form">
                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="names" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Full name*"  required/></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-email">
                                                                                <span class="wpcf7-form-control-wrap your-email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail address*" required /></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-text">
                                                                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-required="true" aria-invalid="false" placeholder="Subject*"  required/></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-text">
                                                                                <span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Write your message here" required></textarea></span>
                                                                            </div>

                                                                            <p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
                                                                        </div>
                                                                    </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('layouts.footer')
@endsection