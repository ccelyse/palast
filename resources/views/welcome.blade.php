@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')

    @include('layouts.topmenu')
    <style>
        .vc_custom_1520940084230 {
            padding-top: 15px !important;
            padding-bottom: 0 !important;
        }
        .vc_custom_1521718761454{
            padding-top: 0px !important;
        }
        .vc_custom_1522139090006{
            padding-top: 0px !important;
        }
        .eltd-tours-row.eltd-tours-columns-4 .eltd-tours-list-grid-sizer, .eltd-tours-row.eltd-tours-columns-4 .eltd-tours-row-item {
            width: 24% !important;
        }
        .rev_slider .tp-caption, .rev_slider .caption {
            top: 30px !important;
        }
    </style>
    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            <div class="eltd-full-width">
                <div class="eltd-full-width-inner">
                    <div class="eltd-grid-row">
                        <div class="eltd-page-content-holder eltd-grid-col-12">
                            <div class="eltd-row-grid-section-wrapper "  >
                                <div class="eltd-row-grid-section">
                                    <div class="vc_row wpb_row vc_row-fluid" >
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner vc_custom_1520942720621">
                                                <div class="wpb_wrapper">
                                                    <div class="eltd-filter-slider">
                                                        <div class="eltd-filter-rev-holder">
                                                            <div class="wpb_revslider_element wpb_content_element">
                                                                <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background:transparent;padding:0px;">
                                                                    <!-- START REVOLUTION SLIDER 5.4.7.2 fullscreen mode -->
                                                                    <div id="rev_slider_11_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.7.2">
                                                                        <ul>
                                                                            <!-- SLIDE  -->
                                                                            @foreach($listslider as $sliders)
                                                                            <li data-index="rs-{{$sliders->id}}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                                <!-- MAIN IMAGE -->
                                                                                <img src="SliderImage/{{$sliders->sliderimage}}"  alt="a" title="{{$sliders->slidercaption}}"  width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                                <!-- LAYERS -->
                                                                                <!-- LAYER NR. 1 -->
                                                                                <div class="tp-caption   tp-resizeme"
                                                                                     id="slide-{{$sliders->id}}"
                                                                                     data-x="['left','left','left','left']" data-hoffset="['1421','976','536','271']"
                                                                                     data-y="['top','top','top','top']" data-voffset="['121','74','170','128']"
                                                                                     data-width="none"
                                                                                     data-height="none"
                                                                                     data-whitespace="nowrap"
                                                                                     data-type="image"
                                                                                     data-responsive_offset="on"
                                                                                     data-frames='[{"delay":700,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                     data-paddingtop="[0,0,0,0]"
                                                                                     data-paddingright="[0,0,0,0]"
                                                                                     data-paddingbottom="[0,0,0,0]"
                                                                                     data-paddingleft="[0,0,0,0]"
                                                                                     style="z-index: 5;">
                                                                                    {{--<img src="frontend/assets/images/34892809986_e6d8121ef9_k.jpg" alt="a" data-ww="['111px','87px','64px','32px']" data-hh="['127px','99px','73px','36px']" width="317" height="361" data-no-retina>--}}
                                                                                </div>
                                                                                <div class="tp-caption"
                                                                                     id="slide-{{$sliders->id}}"
                                                                                     data-x="['center','center','center','center']" data-hoffset="['47','145','40','0']"
                                                                                     data-y="['middle','middle','middle','middle']" data-voffset="['5','10','-47','-31']"
                                                                                     data-fontsize="['110','90','80','50']"
                                                                                     data-lineheight="['115','115','115','60']"
                                                                                     data-width="['948','1100','806','319']"
                                                                                     data-height="['120','231','118','122']"
                                                                                     data-whitespace="normal"
                                                                                     data-type="text"
                                                                                     data-responsive_offset="on"
                                                                                     data-responsive="off"
                                                                                     data-frames='[{"delay":100,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                                                                                     data-textAlign="['inherit','inherit','inherit','center']"
                                                                                     data-paddingtop="[0,0,0,0]"
                                                                                     data-paddingright="[0,0,0,0]"
                                                                                     data-paddingbottom="[0,0,0,0]"
                                                                                     data-paddingleft="[0,0,0,0]"
                                                                                     style="z-index: 10; min-width: 948px; max-width: 948px; max-width: 120px; max-width: 120px; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">{{$sliders->slidercaption}} </div>
                                                                                <!-- LAYER NR. 7 -->
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-23"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['0','0','14','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['115','35','66','54']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 11; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">tourist. </div>--}}
                                                                            </li>
                                                                            @endforeach

                                                                            {{--<li data-index="rs-16" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                                                                                {{--<!-- MAIN IMAGE -->--}}
                                                                                {{--<img src="images/palast2.jpeg"  alt="a" title="h1-slide-1"  width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>--}}
                                                                                {{--<!-- LAYERS -->--}}
                                                                                {{--<!-- LAYER NR. 1 -->--}}
                                                                                {{--<div class="tp-caption   tp-resizeme"--}}
                                                                                     {{--id="slide-16-layer-24"--}}
                                                                                     {{--data-x="['left','left','left','left']" data-hoffset="['1421','976','536','271']"--}}
                                                                                     {{--data-y="['top','top','top','top']" data-voffset="['121','74','170','128']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="nowrap"--}}
                                                                                     {{--data-type="image"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-frames='[{"delay":700,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 5;">--}}
                                                                                    {{--<img src="frontend/assets/images/34892809986_e6d8121ef9_k.jpg" alt="a" data-ww="['111px','87px','64px','32px']" data-hh="['127px','99px','73px','36px']" width="317" height="361" data-no-retina>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-27"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['47','145','40','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['5','10','-47','-31']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="['948','1100','806','319']"--}}
                                                                                     {{--data-height="['120','231','118','122']"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":100,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','center']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 10; min-width: 948px; max-width: 948px; max-width: 120px; max-width: 120px; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">Be a traveler, not a  </div>--}}
                                                                                {{--<!-- LAYER NR. 7 -->--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-28"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['0','0','14','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['115','35','66','54']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 11; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">tourist. </div>--}}
                                                                            {{--</li>--}}

                                                                            {{--<li data-index="rs-16" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                                                                                {{--<!-- MAIN IMAGE -->--}}
                                                                                {{--<img src="frontend/assets/images/MountainGorilla-compressed.jpg"  alt="a" title="h1-slide-1"  width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>--}}
                                                                                {{--<!-- LAYERS -->--}}
                                                                                {{--<!-- LAYER NR. 1 -->--}}
                                                                                {{--<div class="tp-caption   tp-resizeme"--}}
                                                                                     {{--id="slide-16-layer-10"--}}
                                                                                     {{--data-x="['left','left','left','left']" data-hoffset="['1421','976','536','271']"--}}
                                                                                     {{--data-y="['top','top','top','top']" data-voffset="['121','74','170','128']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="nowrap"--}}
                                                                                     {{--data-type="image"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-frames='[{"delay":700,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 5;">--}}
                                                                                    {{--<img src="frontend/assets/images/34892809986_e6d8121ef9_k.jpg" alt="a" data-ww="['111px','87px','64px','32px']" data-hh="['127px','99px','73px','36px']" width="317" height="361" data-no-retina>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-1"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['47','145','40','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['5','10','-47','-31']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="['948','1100','806','319']"--}}
                                                                                     {{--data-height="['120','231','118','122']"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":100,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','center']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 10; min-width: 948px; max-width: 948px; max-width: 120px; max-width: 120px; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">Be a traveler, not a  </div>--}}
                                                                                {{--<!-- LAYER NR. 7 -->--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-3"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['0','0','14','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['115','35','66','54']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 11; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">tourist. </div>--}}
                                                                            {{--</li>--}}
                                                                            {{--<li data-index="rs-17" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                                                                            {{--<!-- MAIN IMAGE -->--}}
                                                                            {{--<img src="frontend/assets/images/IMG_0098.jpg"  alt="a" title="h1-slide-1"  width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>--}}
                                                                            {{--<!-- LAYERS -->--}}
                                                                            {{--<!-- LAYER NR. 1 -->--}}
                                                                            {{--<div class="tp-caption   tp-resizeme"--}}
                                                                            {{--id="slide-16-layer-10"--}}
                                                                            {{--data-x="['left','left','left','left']" data-hoffset="['1421','976','536','271']"--}}
                                                                            {{--data-y="['top','top','top','top']" data-voffset="['121','74','170','128']"--}}
                                                                            {{--data-width="none"--}}
                                                                            {{--data-height="none"--}}
                                                                            {{--data-whitespace="nowrap"--}}
                                                                            {{--data-type="image"--}}
                                                                            {{--data-responsive_offset="on"--}}
                                                                            {{--data-frames='[{"delay":700,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                                                            {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                            {{--data-paddingtop="[0,0,0,0]"--}}
                                                                            {{--data-paddingright="[0,0,0,0]"--}}
                                                                            {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                            {{--data-paddingleft="[0,0,0,0]"--}}
                                                                            {{--style="z-index: 5;">--}}
                                                                            {{--<img src="frontend/assets/images/IMG_0098.jpg" alt="a" data-ww="['111px','87px','64px','32px']" data-hh="['127px','99px','73px','36px']" width="317" height="361" data-no-retina>--}}
                                                                            {{--</div>--}}
                                                                            {{--<div class="tp-caption  "--}}
                                                                            {{--id="slide-16-layer-1"--}}
                                                                            {{--data-x="['center','center','center','center']" data-hoffset="['47','145','40','0']"--}}
                                                                            {{--data-y="['middle','middle','middle','middle']" data-voffset="['5','10','-47','-31']"--}}
                                                                            {{--data-fontsize="['110','90','80','50']"--}}
                                                                            {{--data-lineheight="['115','115','115','60']"--}}
                                                                            {{--data-width="['948','1100','806','319']"--}}
                                                                            {{--data-height="['120','231','118','122']"--}}
                                                                            {{--data-whitespace="normal"--}}
                                                                            {{--data-type="text"--}}
                                                                            {{--data-responsive_offset="on"--}}
                                                                            {{--data-responsive="off"--}}
                                                                            {{--data-frames='[{"delay":100,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                            {{--data-textAlign="['inherit','inherit','inherit','center']"--}}
                                                                            {{--data-paddingtop="[0,0,0,0]"--}}
                                                                            {{--data-paddingright="[0,0,0,0]"--}}
                                                                            {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                            {{--data-paddingleft="[0,0,0,0]"--}}
                                                                            {{--style="z-index: 10; min-width: 948px; max-width: 948px; max-width: 120px; max-width: 120px; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">Be a traveler, not a  </div>--}}
                                                                            {{--<!-- LAYER NR. 7 -->--}}
                                                                            {{--<div class="tp-caption  "--}}
                                                                            {{--id="slide-16-layer-3"--}}
                                                                            {{--data-x="['center','center','center','center']" data-hoffset="['0','0','14','0']"--}}
                                                                            {{--data-y="['middle','middle','middle','middle']" data-voffset="['115','35','66','54']"--}}
                                                                            {{--data-fontsize="['110','90','80','50']"--}}
                                                                            {{--data-lineheight="['115','115','115','60']"--}}
                                                                            {{--data-width="none"--}}
                                                                            {{--data-height="none"--}}
                                                                            {{--data-whitespace="normal"--}}
                                                                            {{--data-type="text"--}}
                                                                            {{--data-responsive_offset="on"--}}
                                                                            {{--data-responsive="off"--}}
                                                                            {{--data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                            {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                            {{--data-paddingtop="[0,0,0,0]"--}}
                                                                            {{--data-paddingright="[0,0,0,0]"--}}
                                                                            {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                            {{--data-paddingleft="[0,0,0,0]"--}}
                                                                            {{--style="z-index: 11; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">tourist. </div>--}}
                                                                            {{--</li>--}}

                                                                            {{--<li data-index="rs-17" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                                                                                {{--<!-- MAIN IMAGE -->--}}
                                                                                {{--<img src="frontend/assets/images/DSC_7255.jpg"  alt="a" title="h1-slide-1"  width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>--}}
                                                                                {{--<!-- LAYERS -->--}}
                                                                                {{--<!-- LAYER NR. 1 -->--}}
                                                                                {{--<div class="tp-caption   tp-resizeme"--}}
                                                                                     {{--id="slide-16-layer-10"--}}
                                                                                     {{--data-x="['left','left','left','left']" data-hoffset="['1421','976','536','271']"--}}
                                                                                     {{--data-y="['top','top','top','top']" data-voffset="['121','74','170','128']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="nowrap"--}}
                                                                                     {{--data-type="image"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-frames='[{"delay":700,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 5;">--}}
                                                                                    {{--<img src="frontend/assets/images/DSC_7255.jpg" alt="a" data-ww="['111px','87px','64px','32px']" data-hh="['127px','99px','73px','36px']" width="317" height="361" data-no-retina>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-1"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['47','145','40','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['5','10','-47','-31']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="['948','1100','806','319']"--}}
                                                                                     {{--data-height="['120','231','118','122']"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":100,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','center']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 10; min-width: 948px; max-width: 948px; max-width: 120px; max-width: 120px; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">Be a traveler, not a  </div>--}}
                                                                                {{--<!-- LAYER NR. 7 -->--}}
                                                                                {{--<div class="tp-caption  "--}}
                                                                                     {{--id="slide-16-layer-3"--}}
                                                                                     {{--data-x="['center','center','center','center']" data-hoffset="['0','0','14','0']"--}}
                                                                                     {{--data-y="['middle','middle','middle','middle']" data-voffset="['115','35','66','54']"--}}
                                                                                     {{--data-fontsize="['110','90','80','50']"--}}
                                                                                     {{--data-lineheight="['115','115','115','60']"--}}
                                                                                     {{--data-width="none"--}}
                                                                                     {{--data-height="none"--}}
                                                                                     {{--data-whitespace="normal"--}}
                                                                                     {{--data-type="text"--}}
                                                                                     {{--data-responsive_offset="on"--}}
                                                                                     {{--data-responsive="off"--}}
                                                                                     {{--data-frames='[{"delay":300,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'--}}
                                                                                     {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                                                                     {{--data-paddingtop="[0,0,0,0]"--}}
                                                                                     {{--data-paddingright="[0,0,0,0]"--}}
                                                                                     {{--data-paddingbottom="[0,0,0,0]"--}}
                                                                                     {{--data-paddingleft="[0,0,0,0]"--}}
                                                                                     {{--style="z-index: 11; white-space: normal; font-size: 110px; line-height: 115px; font-weight: 400; color: #fff; letter-spacing: -2px;font-family:quentin;">tourist. </div>--}}
                                                                            {{--</li>--}}
                                                                        </ul>
                                                                        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                                    </div>
                                                                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                                                        if(htmlDiv) {
                                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                                        }else{
                                                                            var htmlDiv = document.createElement("div");
                                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                                        }
                                                                    </script>
                                                                    <script type="text/javascript">
                                                                        if (setREVStartSize!==undefined) setREVStartSize(
                                                                            {c: '#rev_slider_11_1', responsiveLevels: [1920,1441,1025,480], gridwidth: [1300,800,600,300], gridheight: [1100,520,800,500], sliderLayout: 'fullscreen', fullScreenAutoWidth:'off', fullScreenAlignForce:'off', fullScreenOffsetContainer:'', fullScreenOffset:''});

                                                                        var revapi11,
                                                                            tpj;
                                                                        (function() {
                                                                            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();
                                                                            function onLoad() {
                                                                                if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                                                                                if(tpj("#rev_slider_11_1").revolution == undefined){
                                                                                    revslider_showDoubleJqueryError("#rev_slider_11_1");
                                                                                }else{
                                                                                    revapi11 = tpj("#rev_slider_11_1").show().revolution({
                                                                                        sliderType:"standard",
                                                                                        jsFileLocation:"frontend/assets/js/",
                                                                                        sliderLayout:"fullscreen",
                                                                                        dottedOverlay:"none",
                                                                                        delay:4000,
                                                                                        navigation: {
                                                                                            onHoverStop:"off",
                                                                                        },
                                                                                        responsiveLevels:[1920,1441,1025,480],
                                                                                        visibilityLevels:[1920,1441,1025,480],
                                                                                        gridwidth:[1300,800,600,300],
                                                                                        gridheight:[1100,520,800,500],
                                                                                        lazyType:"none",
                                                                                        shadow:0,
                                                                                        spinner:"off",
                                                                                        stopLoop:"off",
                                                                                        stopAfterLoops:-1,
                                                                                        stopAtSlide:-1,
                                                                                        shuffle:"off",
                                                                                        autoHeight:"off",
                                                                                        fullScreenAutoWidth:"off",
                                                                                        fullScreenAlignForce:"off",
                                                                                        fullScreenOffsetContainer: "",
                                                                                        fullScreenOffset: "",
                                                                                        disableProgressBar:"on",
                                                                                        hideThumbsOnMobile:"off",
                                                                                        hideSliderAtLimit:0,
                                                                                        hideCaptionAtLimit:0,
                                                                                        hideAllCaptionAtLilmit:0,
                                                                                        debugMode:false,
                                                                                        fallbacks: {
                                                                                            simplifyAll:"off",
                                                                                            nextSlideOnWindowFocus:"off",
                                                                                            disableFocusListener:false,
                                                                                        }
                                                                                    });
                                                                                }; /* END OF revapi call */

                                                                            }; /* END OF ON LOAD FUNCTION */
                                                                        }()); /* END OF WRAPPING FUNCTION */
                                                                    </script>
                                                                </div>
                                                                <!-- END REVOLUTION SLIDER -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="eltd-row-grid-section-wrapper "  >
                                <div class="eltd-row-grid-section">
                                    <div class="vc_row wpb_row vc_row-fluid" >
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >
                                                        <div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-5770" data-1280-1600="9% 18% 0%" data-1024-1280="9% 20% 0%" data-768-1024="0 10%" data-680-768="0 10%" data-680="0%">
                                                            <div class="eltd-eh-item-inner">
                                                                <div class="eltd-eh-item-content eltd-eh-custom-5770" style="padding: 0 20%">
                                                                    <div class="eltd-section-title-holder   text-align-center eltd-animated-separator" style="text-align: center">
                                                                        <div class="eltd-st-inner">
                                                                            <h2 class="eltd-st-title" style="color: #ff9027">
                                                                                Discover our destination list
                                                                            </h2>
                                                                            <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                                <div class="eltd-separator" style="border-color: #000000"></div>
                                                                            </div>
                                                                            <h5 class="eltd-st-text" style="line-height: 30px;font-weight: 300">
                                                                                {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. --}}
                                                                            </h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="eltd-row-grid-section-wrapper "  >
                                <div class="eltd-row-grid-section">
                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1520940084230" >
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="eltd-tours-destination-grid">
                                                        <div class="eltd-tours-destination-holder eltd-tours-row eltd-tours-columns-4 eltd-small-space">
                                                            <div class="eltd-tours-row-inner-holder eltd-outer-space">

                                                                @foreach($listattractions as $destination)
                                                                    <div class="eltd-tours-destination-grid-item eltd-tours-row-item eltd-item-space post-230 destinations type-destinations status-publish has-post-thumbnail hentry">
                                                                        <div class="eltd-tours-destination-item-holder">
                                                                            <a href="{{ route('ActivityMore',['id'=> $destination->id])}}">
                                                                                <div class="eltd-tours-destination-item-image">
                                                                                    <img width="800" height="790" src="ActivityCoverImage/{{$destination->activityimage}}" class="attachment-full size-full wp-post-image" alt="a" srcset="ActivityCoverImage/{{$destination->activityimage}}" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                </div>
                                                                                <div class="eltd-tours-destination-item-content">
                                                                                    <div class="eltd-tours-destination-item-content-inner">
                                                                                        <h4 class="eltd-tours-destination-item-title">{{$destination->activitytitle}}</h4>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="eltd-row-grid-section-wrapper "  >--}}
                            {{--<div class="eltd-row-grid-section">--}}
                            {{--<div class="vc_row wpb_row vc_row-fluid" >--}}
                            {{--<div class="wpb_column vc_column_container vc_col-sm-12">--}}
                            {{--<div class="vc_column-inner ">--}}
                            {{--<div class="wpb_wrapper">--}}
                            {{--<div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >--}}
                            {{--<div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-5770" data-1280-1600="9% 18% 0%" data-1024-1280="9% 20% 0%" data-768-1024="0 10%" data-680-768="0 10%" data-680="0%">--}}
                            {{--<div class="eltd-eh-item-inner">--}}
                            {{--<div class="eltd-eh-item-content eltd-eh-custom-5770" style="padding: 0 20%">--}}
                            {{--<div class="eltd-section-title-holder   text-align-center eltd-animated-separator" style="text-align: center">--}}
                            {{--<div class="eltd-st-inner">--}}
                            {{--<h2 class="eltd-st-title" style="color: #ff9027">--}}
                            {{--We choose for you--}}
                            {{--</h2>--}}
                            {{--<div class="eltd-separator-holder clearfix  eltd-separator-center ">--}}
                            {{--<div class="eltd-separator" style="border-color: #000000"></div>--}}
                            {{--</div>--}}
                            {{--<h5 class="eltd-st-text" style="line-height: 30px;font-weight: 300">--}}
                            {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. --}}
                            {{--</h5>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="vc_row wpb_row vc_row-fluid vc_custom_1521718761454 vc_row-has-fill" >--}}
                            {{--<div class="wpb_column vc_column_container vc_col-sm-12">--}}
                            {{--<div class="vc_column-inner ">--}}
                            {{--<div class="wpb_wrapper">--}}
                            {{--<div class="wpb_wrapper">--}}
                            {{--<div class="eltd-row-grid-section-wrapper eltd-content-aligment-left" >--}}
                            {{--<div class="eltd-row-grid-section">--}}
                            {{--<div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1521192359252" >--}}
                            {{--<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">--}}
                            {{--<div class="vc_column-inner vc_custom_1521535148454">--}}
                            {{--<div class="wpb_wrapper">--}}
                            {{--<div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >--}}
                            {{--<div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-7097" data-1280-1600="0 0 0 0" data-1024-1280="0 0 0 0" data-768-1024="0 0 0 0" data-680-768="0 0 0 0" data-680="0 0 0 0">--}}
                            {{--<div class="eltd-eh-item-inner">--}}
                            {{--<div class="eltd-eh-item-content eltd-eh-custom-7097" style="padding: 12px 0 0 0 ">--}}
                            {{--<div class="eltd-section-title-holder   text-align-left eltd-animated-separator" style="text-align: left">--}}
                            {{--<div class="eltd-st-inner">--}}
                            {{--<h2 class="eltd-st-title" style="color: #ff9027">--}}
                            {{--Top Itineraries--}}
                            {{--</h2>--}}
                            {{--<div class="eltd-separator-holder clearfix  eltd-separator-center ">--}}
                            {{--<div class="eltd-separator" style="border-color: #000000"></div>--}}
                            {{--</div>--}}
                            {{--<p class="eltd-st-text" style="color: #565656;margin-top: 34px">--}}
                            {{--Lorem ipsum dolor sit amet, consectetur adi- piscing elit. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum--}}
                            {{--</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">--}}
                            {{--<div class="vc_column-inner vc_custom_1521192355304">--}}
                            {{--<div class="wpb_wrapper">--}}
                            {{--<div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >--}}
                            {{--<div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-1227" data-1280-1600="10px 0 0 0" data-1024-1280="10px 0 0 0" data-768-1024="15px 0 0 0" data-680-768="15px 0 0 0" data-680="15px 0 0 0">--}}
                            {{--<div class="eltd-eh-item-inner">--}}
                            {{--<div class="eltd-eh-item-content eltd-eh-custom-1227" style="padding: 12px 0 0 0">--}}
                            {{--<div class="eltd-video-button-holder  eltd-vb-has-img">--}}
                            {{--<div class="eltd-video-button-image">--}}
                            {{--<img width="800" height="495" src="frontend/assets/images/02/h1-img-1.jpg" class="attachment-full size-full" alt="a" srcset="frontend/assets/images/02/h1-img-1.jpg 800w, frontend/assets/images/02/h1-img-1-508x314.jpg 508w, frontend/assets/images/02/h1-img-1-300x186.jpg 300w, frontend/assets/images/02/h1-img-1-768x475.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" />--}}
                            {{--</div>--}}
                            {{--<a class="eltd-video-button-play-custom " href="https://vimeo.com/143023052" target="_self" data-rel="prettyPhoto[video_button_pretty_photo_206]">--}}
                            {{--<div class="eltd-vb-icon-holder">--}}
                            {{--<div class="eltd-vb-background">--}}
                            {{--<div class="eltd-vb-play"></div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="vc_empty_space"   style="height: 40px" ><span class="vc_empty_space_inner"></span></div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <footer class="eltd-page-footer">
                                {{--<div class="eltd-footer-top-holder">--}}
                                    {{--<div class="eltd-footer-top-inner eltd-grid">--}}
                                        {{--<div class="eltd-grid-row eltd-footer-top-alignment-center eltd-footer-top-skin-dark">--}}
                                            {{--<div class="eltd-column-content eltd-grid-col-12">--}}
                                                {{--<div class="widget eltd-custom-font-widget">--}}
                                                    {{--<h2 class="eltd-custom-font-holder  eltd-cf-6069  " style="color:#ff9027; font-family: quentin;font-size: 60px;letter-spacing: 2.4px;text-transform: initial;text-align: center" data-item-class="eltd-cf-6069" data-font-size-680="48px">--}}
                                                        {{--Make memories with us--}}
                                                    {{--</h2>--}}
                                                {{--</div>--}}
                                                {{--<div class="widget eltd-separator-widget">--}}
                                                    {{--<div class="eltd-separator-holder clearfix  eltd-separator-center eltd-separator-normal">--}}
                                                        {{--<div class="eltd-separator" style="border-style: solid;width: 93px;margin-top: -8px"></div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<a class="eltd-social-icon-widget-holder eltd-icon-has-hover"  style="font-size: 26px;margin: 41px 28px 53px 0;" href="#" target="_blank">--}}
                                                    {{--<span class="eltd-social-icon-widget   ion-social-facebook   "></span>		</a>--}}
                                                {{--<a class="eltd-social-icon-widget-holder eltd-icon-has-hover"  style="font-size: 20px;margin: 41px 28px 53px 0;" href="#" target="_blank">--}}
                                                    {{--<span class="eltd-social-icon-widget   ion-social-youtube   "></span>		</a>--}}
                                                {{--<a class="eltd-social-icon-widget-holder eltd-icon-has-hover"  style="font-size: 22px;margin: 42px 28px 53px 0;" href="#" target="_blank">--}}
                                                    {{--<span class="eltd-social-icon-widget   ion-social-twitter   "></span>		</a>--}}
                                                {{--<a class="eltd-social-icon-widget-holder eltd-icon-has-hover"  style="font-size: 24px;margin: 42px 0px 53px 0px;" href="#" target="_blank">--}}
                                                    {{--<span class="eltd-social-icon-widget   ion-social-instagram   "></span>		</a>--}}
                                                {{--<div id="eltd_instagram_widget-4" class="widget eltd-footer-column-1 widget_eltd_instagram_widget">--}}
                                                    {{--<ul class="eltd-instagram-feed clearfix eltd-col-6 eltd-instagram-carousel eltd-owl-slider" data-number-of-items="6" data-slider-margin="20" data-enable-navigation="yes" data-enable-pagination="no">--}}
                                                        {{--<li>--}}
                                                            {{--<a href="#" target="_blank">--}}
                                                                {{--<img class="thumbnail" src="images/palast5.jpeg" alt="" width="400" height="400" />						</a>--}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<a href="#" target="_blank">--}}
                                                                {{--<img class="thumbnail" src="images/palast2.jpeg" alt="" width="400" height="400" />						</a>--}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<a href="#" target="_blank">--}}
                                                                {{--<img class="thumbnail" src="images/palast4.jpeg" alt="" width="400" height="400" />						</a>--}}
                                                        {{--</li>--}}


                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </footer>
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1522139090006 vc_row-has-fill" >
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >
                                                <div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-9722" data-1280-1600="0 22.5%" data-1024-1280="0 19.5%" data-768-1024="0 12.5%" data-680-768="0 12.5%" data-680="0% 10%">
                                                    <div class="eltd-eh-item-inner">
                                                        <div class="eltd-eh-item-content eltd-eh-custom-9722" style="padding: 0 27.5%">
                                                            <div class="eltd-section-title-holder   text-align-center eltd-animated-separator" style="text-align: center">
                                                                <div class="eltd-st-inner">
                                                                    <h2 class="eltd-st-title" style="color: #ff9027">
                                                                        Contact Us
                                                                    </h2>
                                                                    <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                        <div class="eltd-separator" style="border-color: #000000"></div>
                                                                    </div>
                                                                    <h5 class="eltd-st-text" style="color: #565656">
                                                                        {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. --}}
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space"   style="height: 61px" ><span class="vc_empty_space_inner"></span></div>


                                                                <form action="{{url('SendingEmail')}}" method="post" class="wpcf7-form cf7_custom_style_1" enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <div class="eltd-contact-form">
                                                                        <div class="eltd-contact-form-name">
                                                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="names" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Full name*"  required/></span>
                                                                        </div>
                                                                        <div class="eltd-contact-form-email">
                                                                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail address*" required /></span>
                                                                        </div>
                                                                        <div class="eltd-contact-form-text">
                                                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-required="true" aria-invalid="false" placeholder="Subject*"  required/></span>
                                                                        </div>
                                                                        <div class="eltd-contact-form-text">
                                                                            <span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="6" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Write your message here" required></textarea></span>
                                                                        </div>

                                                                        <p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
                                                                    </div>
                                                                </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- close div.content_inner -->
    </div>
    <!-- close div.content -->
    @include('layouts.footer')

@endsection


