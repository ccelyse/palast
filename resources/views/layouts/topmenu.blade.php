<div class="eltd-wrapper">
    <div class="eltd-wrapper-inner">
        <div class="eltd-top-bar">
            <div class="eltd-vertical-align-containers">
                <div class="eltd-position-left">
                    <div class="eltd-position-left-inner">
                        <div id="text-3" class="widget widget_text eltd-top-bar-widget">
                            <div class="textwidget">
                                <p style="padding-top:13px !important;">Leisure beyond your expectations</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="eltd-position-right">
                    <div class="eltd-position-right-inner">
                        {{--<div id="text-4" class="widget widget_text eltd-top-bar-widget">--}}
                            {{--<div class="textwidget">--}}
                                {{--<p>Highest quality of service for 2019</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <a class="eltd-icon-widget-holder"  href="tel:381123456789" target="_self" style="margin: 0 24px 0 8px">
                            <span class="eltd-icon-element fa fa-phone"></span>			<span class="eltd-icon-text ">+250 78831 6763</span>		</a>
                        <a class="eltd-icon-widget-holder"  href="mailto:info@bonvoyage.com" target="_self" style="margin: 0 20px 0 0">
                            <span class="eltd-icon-element fa fa-envelope"></span>			<span class="eltd-icon-text ">info@palasttours.rw</span>		</a>

                        <div class="widget eltd-separator-widget">
                            <div class="eltd-separator-holder clearfix  eltd-separator-center eltd-separator-normal">
                                <div class="eltd-separator" style="border-style: solid;width: 8px"></div>
                            </div>
                        </div>
                        <div class="widget eltd-login-register-widget"><a href="{{url('login')}}">
                                <span class="eltd-login-text">Sign In</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-fullscreen-search-holder">
            
            <div class="eltd-fullscreen-search-table">
                <div class="eltd-fullscreen-search-cell">
                    <div class="eltd-fullscreen-search-inner">
                        <form action="#" class="eltd-fullscreen-search-form" method="get">
                            <div class="eltd-form-holder">
                                <div class="eltd-form-holder-inner">
                                    <div class="eltd-field-holder">
                                        <input type="text" placeholder="Type your search here" name="s" class="eltd-search-field" autocomplete="off"/>
                                        <button type="submit" class="eltd-search-submit">Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <header class="eltd-page-header">
            <div class="eltd-menu-area eltd-menu-right">
                <div class="eltd-vertical-align-containers">
                    <div class="eltd-position-left">
                        <div class="eltd-position-left-inner">
                            <div class="eltd-logo-wrapper">
                                <a itemprop="url" href="{{url('/')}}" style="height: 23px;">
                                    <img itemprop="image" class="eltd-normal-logo" src="frontend/assets/images/logo.png" width="380" height="46"  alt="logo"/>
                                    <img itemprop="image" class="eltd-dark-logo" src="frontend/assets/images/logo.png" width="380" height="46"  alt="dark logo"/>
                                    <img itemprop="image" class="eltd-light-logo" src="frontend/assets/images/logo.png" width="380" height="46"  alt="light logo"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="eltd-position-right">
                        <div class="eltd-position-right-inner">
                            <nav class="eltd-main-menu eltd-drop-down eltd-default-nav">
                                <ul id="menu-main-menu" class="clearfix">
                                    <li id="nav-menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub narrow">
                                        <a href="{{url('/')}}" class=""><span class="item_outer"><span class="item_text">Home</span></span></a>
                                    </li>
                                    <li id="sticky-nav-menu-item-1528" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="{{url('About')}}" class=""><span class="item_outer"><span class="item_text">About Us</span><span class="plus"></span><i class="eltd-menu-arrow fa fa-angle-down"></i></span></a>
                                        {{--<div class="second ">--}}
                                            {{--<div class="inner" >--}}
                                                {{--<ul >--}}
                                                    {{--<li id="sticky-nav-menu-item-1529" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="#" class=""><span class="item_outer"><span class="item_text">Our Team</span><span class="plus"></span></span></a></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="second ">--}}
                                            {{--<div class="inner" >--}}
                                                {{--<ul >--}}
                                                    {{--<li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('OurFleet')}}" class=""><span class="item_outer"><span class="item_text">Our Fleet</span><span class="plus"></span></span></a></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </li>
                                    <li id="sticky-nav-menu-item-1528" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=""><span class="item_outer"><span class="item_text">Our services</span><span class="plus"></span><i class="eltd-menu-arrow fa fa-angle-down"></i></span></a>
                                        <div class="second ">
                                            <div class="inner" >
                                                <ul >
                                                    <li id="sticky-nav-menu-item-1529" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="{{url('FlightBookings')}}" class=""><span class="item_outer"><span class="item_text">Flight booking and vacation packages</span><span class="plus"></span></span></a></li>
                                                    <li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('SafariExperiences')}}" class=""><span class="item_outer"><span class="item_text">Safari Experiences</span><span class="plus"></span></span></a></li>
                                                    {{--<li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('PalastAssist')}}" class=""><span class="item_outer"><span class="item_text">Palast Assist : Airport Transfers</span><span class="plus"></span></span></a></li>--}}
                                                    <li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('OurFleets')}}" class=""><span class="item_outer"><span class="item_text">Our Fleet</span><span class="plus"></span></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    {{--<li id="nav-menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub narrow">--}}
                                        {{--<a href="{{url('Destinations')}}" class=""><span class="item_outer"><span class="item_text">Destinations</span></span></a>--}}
                                    {{--</li>--}}
                                    {{--<li id="nav-menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub narrow">--}}
                                        {{--<a href="#" class=""><span class="item_outer"><span class="item_text">Itineraries</span></span></a>--}}
                                    {{--</li>--}}
                                    <li id="nav-menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub narrow">
                                        <a href="{{url('ContactUs')}}" class=""><span class="item_outer"><span class="item_text">Contact Us</span></span></a>
                                    </li>

                                </ul>
                            </nav>

                        
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <header class="eltd-mobile-header">
            <div class="eltd-mobile-header-inner">
                <div class="eltd-mobile-header-holder">
                    <div class="eltd-grid">
                        <div class="eltd-vertical-align-containers">
                            <div class="eltd-vertical-align-containers">
                                <div class="eltd-mobile-menu-opener">
                                    <a href="javascript:void(0)">
                                 <span class="eltd-mobile-menu-icon">
                                 <span aria-hidden="true" class="eltd-icon-font-elegant icon_menu " ></span>									</span>
                                    </a>
                                </div>
                                <div class="eltd-position-center">
                                    <div class="eltd-position-center-inner">
                                        <div class="eltd-mobile-logo-wrapper">
                                            <a itemprop="url" href="{{url('/')}}" style="height: 14px">
                                                <img itemprop="image" src="frontend/assets/images/02/logo-mobile.png" width="240" height="29"  alt="Mobile Logo"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltd-position-right">
                                    <div class="eltd-position-right-inner">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="eltd-mobile-nav">
                    <div class="eltd-grid">
                        <ul id="menu-main-menu-2" class="">
                            <li id="mobile-menu-item-219" class="menu-item menu-item-type-custom menu-item-object-custom   has_sub">
                                <a href="{{url('/')}}"><span>Home</span></a><span class="mobile_arrow"></span>
                            </li>
                            <li id="sticky-nav-menu-item-1528" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="{{url('AboutUs')}}" class=""><span class="item_outer"><span class="item_text">About Us</span><span class="plus"></span><i class="eltd-menu-arrow fa fa-angle-down"></i></span></a>
                            </li>
                            <li id="sticky-nav-menu-item-1528" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="index.html#" class=""><span class="item_outer"><span class="item_text">Our services</span><span class="plus"></span><i class="eltd-menu-arrow fa fa-angle-down"></i></span></a>
                                <div class="second ">
                                    <div class="inner" >
                                        <ul >
                                            <li id="sticky-nav-menu-item-1529" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="{{url('FlightBooking')}}" class=""><span class="item_outer"><span class="item_text">Flight Booking Services and getaway packages</span><span class="plus"></span></span></a></li>
                                            <li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('SafariExperiences')}}" class=""><span class="item_outer"><span class="item_text">Safari Experiences</span><span class="plus"></span></span></a></li>
                                            <li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('PalastAssist')}}" class=""><span class="item_outer"><span class="item_text">Palast Assist : Airport Transfers</span><span class="plus"></span></span></a></li>
                                            <li id="sticky-nav-menu-item-1530" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="{{url('OurFleet')}}" class=""><span class="item_outer"><span class="item_text">Our Fleet</span><span class="plus"></span></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        
                            {{--<li id="mobile-menu-item-219" class="menu-item menu-item-type-custom menu-item-object-custom has_sub">--}}
                                {{--<a href="{{url('Destinations')}}"><span>Destinations</span></a><span class="mobile_arrow"></span>--}}
                            {{--</li>--}}
                            {{--<li id="mobile-menu-item-219" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub">--}}
                                {{--<a href="#"><span>Itineraries</span></a><span class="mobile_arrow"></span>--}}
                            {{--</li>--}}
                            <li id="mobile-menu-item-219" class="menu-item menu-item-type-custom menu-item-object-custom  has_sub">
                                <a href="{{url('ContactUs')}}"><span>Contact Us</span></a><span class="mobile_arrow"></span>
                            </li>

                        </ul>
                        </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
    