<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes">
    <meta name="author" content="Elysee CONFIANCE" />
    <meta property="og:site_name" content="Palast Tours & Travel" />
    <meta property="og:url" content="http://palasttours.rw" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Palast Tours & Travel" />
    <meta property="og:description" content="#1 Rated Day Tours: Experience local life culture and food. Gorillas, safari and more in Rwanda. Shop our boutique of contemporary Made in Rwanda products!" />
    <meta property="og:image" content="frontend/assets/images/logo.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="#1 Rated Day Tours: Experience local life culture and food. Gorillas, safari and more in Rwanda. Shop our boutique of contemporary Made in Rwanda products!" />
    <meta name="twitter:title" content="Palast Tours & Travel" />
    <meta name="twitter:image" content="frontend/assets/images/logo.png" />
    <title>@yield('title')</title>
    {{--  <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/bonvoyage.elated-themes.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>  --}}
    <style type="text/css">img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet"  href="frontend/assets/styles.css">
    <link rel='stylesheet' id='contact-form-7-css'  href='frontend/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-membership-style-css'  href='frontend/wp-content/plugins/eltd-membership/assets/css/membership.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-membership-responsive-style-css'  href='frontend/wp-content/plugins/eltd-membership/assets/css/membership-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-modules-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/modules.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-tours-style-css'  href='frontend/wp-content/plugins/eltd-tours/assets/css/tours.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-modules-responsive-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/modules-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-tours-responsive-style-css'  href='frontend/wp-content/plugins/eltd-tours/assets/css/tours-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nouislider-css'  href='frontend/wp-content/plugins/eltd-tours/assets/css/nouislider.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='frontend/wp-content/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-default-style-css'  href='frontend/wp-content/themes/bonvoyage/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-font_awesome-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/font-awesome/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-font_elegant-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/elegant-icons/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-ion_icons-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/ion-icons/css/ionicons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-linea_icons-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/linea-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='eltd-linear_icons-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/linear-icons/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css'  href='frontend/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css'  href='frontend/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-woo-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/woocommerce.min.css' type='text/css' media='all' />
    <style id='bonvoyage-elated-woo-inline-css' type='text/css'>
        .page-id-28 .eltd-top-bar { background-color: rgba(0, 0, 0, 1);}
        .page-id-28.eltd-boxed .eltd-wrapper { background-attachment: fixed;}
        .page-id-28 .eltd-page-footer .eltd-footer-top-holder { background-color: #ffffff;}
        .page-id-28 .eltd-page-footer .eltd-footer-bottom-holder { background-color: #ffffff;}
        .page-id-28 .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner, .page-id-28 .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner{ padding-top: 0px !important;}
    </style>
    <link rel='stylesheet' id='bonvoyage-elated-woo-responsive-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/woocommerce-responsive.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-style-dynamic-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/style_dynamic.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-style-dynamic-responsive-css'  href='frontend/wp-content/themes/bonvoyage/assets/css/style_dynamic_responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bonvoyage-elated-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Playfair+Display+SC%3A300%2C400%2C700%7CLato%3A300%2C400%2C700&amp;subset=latin-ext&amp;ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css'  href='frontend/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
    <script type="text/javascript" src="frontend/wp-content/cache/minify/49e40.js"></script>
    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key=AIzaSyBuNUNSUYWtHepSQnlsr7tqk7Gla8i2kqw&amp;ver=4.9.9'></script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/a505f.js"></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/bonvoyage.elated-themes.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/f1253.js"></script>
    <script type='text/javascript'>
        var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
    </script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/864c2.js"></script>
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="ce23fb65-16da-416f-aa31-6e2c9a94ca44";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
   <link rel="stylesheet" type="text/css" href="http://bonvoyage.elated-themes.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
   <script type="text/javascript">function setREVStartSize(e){
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
        };</script>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1520940084230{padding-top: 71px !important;padding-bottom: 110px !important;}.vc_custom_1521718761454{padding-top: 112px !important;padding-bottom: 73px !important;background-image: url("wp-content/uploads/2018/02/h1-background-img-1-id=40.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1521718575868{padding-top: 195px !important;padding-bottom: 100px !important;background-image: url("wp-content/uploads/2018/03/h1-background-img-2-id=2267.jpg") !important;background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1522139090006{padding-top: 82px !important;padding-bottom: 121px !important;background-image: url("wp-content/uploads/2018/02/h1-background-img-3-id=183.jpg") !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1519041232816{padding-top: 100px !important;padding-bottom: 98px !important;}.vc_custom_1520942720621{margin-bottom: -15px !important;}.vc_custom_1521192343401{padding-top: 0px !important;}.vc_custom_1521192359252{padding-top: 0px !important;}.vc_custom_1521535148454{padding-top: 0px !important;padding-right: 11% !important;}.vc_custom_1521192355304{padding-top: 0px !important;}</style><noscript><style type="text/css">.wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="home page-template page-template-full-width page-template-full-width-php page page-id-28 eltd-core-1.0 eltd-social-login-1.0 eltd-tours-1.0 bonvoyage-ver-1.0 eltd-grid-1300 eltd-content-is-behind-header eltd-sticky-header-on-scroll-up eltd-dropdown-animate-height eltd-header-standard eltd-menu-area-shadow-disable eltd-menu-area-in-grid-shadow-disable eltd-menu-area-border-disable eltd-menu-area-in-grid-border-disable eltd-logo-area-border-disable eltd-header-vertical-shadow-disable eltd-header-vertical-border-disable eltd-side-menu-slide-from-right eltd-woocommerce-columns-3 eltd-woo-normal-space eltd-woo-pl-info-below-image eltd-woo-single-thumb-on-left-side eltd-woo-single-has-pretty-photo eltd-default-mobile-header eltd-sticky-up-mobile-header eltd-header-top-enabled wpb-js-composer js-comp-ver-5.4.7 vc_responsive eltd-fullscreen-search eltd-search-fade" itemscope itemtype="http://schema.org/WebPage">

@yield('content')
