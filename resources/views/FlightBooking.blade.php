@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')

    <div class="eltd-content" style="margin-top: -90px">
        @foreach($listflight as $data)
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="height: 520px;background-image:url('frontend/assets/images/photo-1488085061387-422e29b40080.jpeg');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="FlightBooking/{{$data->flightbookingimage}}" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">{{$data->flightbookingcaption}}</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper eltd-content-aligment-center">
                            <div class="eltd-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1519737413376">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 ">
                                                    <div class="eltd-eh-item    " data-item-class="eltd-eh-custom-8750" data-1280-1600="65px 156px 81px" data-1024-1280="65px 156px 81px" data-768-1024="65px 0 81px" data-680-768="65px 0 81px" data-680="65px 0 81px">
                                                        <div class="eltd-eh-item-inner">
                                                            <div class="eltd-eh-item-content eltd-eh-custom-8750" style="padding: 65px 252px 81px">
                                                                <div class="eltd-section-title-holder   eltd-animated-separator">
                                                                    <div class="eltd-st-inner">
                                                                        <h2 class="eltd-st-title">
                                                                            {{$data->flightbookingcaption}}		</h2>
                                                                        <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                            <div class="eltd-separator"></div>
                                                                        </div>
                                                                        <?php
                                                                        $text = $data->flightbookingmore;
                                                                        echo $text;
                                                                        ?>
                                                                        {{--<p class="eltd-st-text">--}}
                                                                            {{--Palast Tours and Travel team assists you to arrange your travel across the world. The team will provide guidance on the suitable travel options to your destination whether it is for business or leisure travel.--}}
                                                                        {{--</p>--}}
                                                                        {{--<p class="eltd-st-text">--}}
                                                                            {{--We also offer personalized getaway packages to it all ages , budgets and tastes from adventure trips, to honeymoon escapes.--}}
                                                                        {{--<p class="eltd-st-text">--}}
                                                                            {{--We operate from Kigali , the capital city , Bugesera in the Eastern Province and we are also accessible online to ensure we provide you with a convenient service.--}}
                                                                        {{--</p>--}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    @endforeach
    </div>
    @include('layouts.footer')
@endsection