@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')

    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important; background-image:url('frontend/assets/images/Africa__0001_Africa-elephant-family-crossing-plains.jpg');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="frontend/assets/images/safariexperience.jpg" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">Pay Now</h1>
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 50px;font-size: 35px;color: #fff;">
                                    {{ session('success') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper eltd-content-aligment-center">
                            <div class="eltd-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1522139090006 vc_row-has-fill" >
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 " >
                                                    <div class="eltd-eh-item    "  data-item-class="eltd-eh-custom-9722" data-1280-1600="0 22.5%" data-1024-1280="0 19.5%" data-768-1024="0 12.5%" data-680-768="0 12.5%" data-680="0% 10%">
                                                        <div class="eltd-eh-item-inner">
                                                            <div class="eltd-eh-item-content eltd-eh-custom-9722">
                                                                <div class="eltd-section-title-holder   text-align-center eltd-animated-separator" style="text-align: center">
                                                                    <div class="eltd-st-inner">
                                                                        {{--<h2 class="eltd-st-title" style="color: #000000">--}}
                                                                            {{--Ask us anything--}}
                                                                        {{--</h2>--}}

                                                                        <h5 class="eltd-st-text" style="color: #565656">
                                                                            {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim. --}}
                                                                        </h5>


                                                                    </div>
                                                                </div>

                                                                    <form action="{{url('MakePayment')}}" method="post" class="wpcf7-form cf7_custom_style_1" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        @foreach($getinfo as $datas)
                                                                        <div class="eltd-contact-form">

                                                                            <div class="eltd-contact-form-name" hidden>
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                         <input type="text" name="booking_id" value="<?php echo $id?>" size="40" class="wpcf7-form-control wpcf7-text"  readonly/>
                                                                                </span>
                                                                            </div>

                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                         <input type="text" name="enquire_country" value="{{$datas->enquire_country}}" size="40" class="wpcf7-form-control wpcf7-text"  readonly/>
                                                                                </span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                         <input type="text" name="enquire_title" value="{{$datas->enquire_title}}" size="40" class="wpcf7-form-control wpcf7-text"  readonly/>
                                                                                </span>
                                                                            </div>

                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                    <input type="text" name="enquire_first_name" value="{{$datas->enquire_first_name}}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"  readonly/>
                                                                                </span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                    <input type="text" name="enquire_last_time" value="{{$datas->enquire_last_time}}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Last name*"  readonly/></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-name">
                                                                                <span class="wpcf7-form-control-wrap your-name">
                                                                                    <input type="text" name="enquire_telephone" value="{{$datas->enquire_telephone}}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Telephone*"  readonly/></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-email" style="padding-left: 0px !important;padding-right: 13px !important;">
                                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                                    <input type="email" name="enquire_email" value="{{$datas->enquire_email}}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail address*" readonly /></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-email" style="padding-left: 0px !important;padding-right: 13px !important;" hidden>
                                                                                <span class="wpcf7-form-control-wrap your-email">
                                                                                    <input type="email" name="booking_date" value="{{$datas->created_at}}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail address*" readonly /></span>
                                                                            </div>
                                                                            <div class="eltd-contact-form-text">
                                                                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="booking_price" value="${{$datas->booking_price}}" size="40" class="wpcf7-form-control wpcf7-text" aria-required="true" aria-invalid="false" placeholder="Subject*"  readonly/></span>
                                                                            </div>


                                                                            <p><input type="submit" value="Pay Now" class="wpcf7-form-control wpcf7-submit" /></p>
                                                                        </div>
                                                                        @endforeach
                                                                    </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('layouts.footer')
@endsection