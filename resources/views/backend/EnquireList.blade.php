@extends('backend.layout.master')

@section('title', 'Palast')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #ff9027 !important;
            border-color: #ff9027 !important;
        }
        .btn-primary:hover{
            background-color: #ff9027 !important;
            border-color:#ff9027 !important;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->


                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        {{--<h4 class="card-title">Destination list</h4>--}}
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Set price</th>
                                                    <th>Enquire code</th>
                                                    <th>Destination</th>
                                                    <th>Title</th>
                                                    <th>First name</th>
                                                    <th>Last name</th>
                                                    <th>Telephone</th>
                                                    <th>Email</th>
                                                    <th>Country</th>
                                                    <th>Subject</th>
                                                    <th>Enquire message</th>
                                                    <th>Send Email</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list as $data)
                                                    <tr>
                                                        <td><a href="{{ route('backend.SetPrice',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Set price</a></td>
                                                        <td>{{$data->enquire_code}}</td>
                                                        <td>{{$data->attraction_name}}</td>
                                                        <td>{{$data->enquire_title}}</td>
                                                        <td>{{$data->enquire_first_name}}</td>
                                                        <td>{{$data->enquire_last_time}}</td>
                                                        <td>{{$data->enquire_telephone}}</td>
                                                        <td>{{$data->enquire_email}}</td>
                                                        <td>{{$data->enquire_country}}</td>
                                                        <td>{{$data->enquire_subject}}</td>
                                                        <td><a href="{{ route('backend.EnquiryEmail',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Send Email</a></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#editsummary{{$data->id}}">
                                                                Enquire message
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Message</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>{{$data->enquire_message}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="#" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
                    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
                    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
                    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
                    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
