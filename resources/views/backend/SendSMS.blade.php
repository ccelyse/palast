@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .textareaform{
            background-color: #fff;
            position: relative;
            display: block;
            width: 100%;
            height: 100px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            text-align: left;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{url('SendSMS_')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="fas fa-comment"></i>Send SMS</h4>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                    <div >
                                                                        <label>Message</label>

                                                                        <script language="javascript" type="text/javascript">
                                                                            function limitText(limitField, limitCount, limitNum) {
                                                                                if (limitField.value.length > limitNum) {
                                                                                    limitField.value = limitField.value.substring(0, limitNum);
                                                                                } else {
                                                                                    limitCount.value = limitNum - limitField.value.length;
                                                                                }
                                                                            }
                                                                        </script>

                                                                        <textarea class="textareaform" name="limitedtextarea" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);"
                                                                                  onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);" rows="10"></textarea><br>
                                                                        <font size="1">(Maximum characters: 160)<br>
                                                                            You have <input readonly type="text" name="countdown" size="3" value="160"> characters left.</font>
                                                                    </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Name</label>
                                                                    <select class="select2 form-control" name="phonenumber">
                                                                        @foreach($listnumber as $data)
                                                                            <option value="{{$data->phonenumber}}">{{$data->companyname}}</option>
                                                                        @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Send</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
