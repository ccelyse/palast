<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class=" nav-item"><a href="#"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Account</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="{{url('CreateAccount')}}"><i class="fas fa-user-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Create Account</span></a>
                    </li>
                    <li class=" nav-item"><a href="{{url('AccountList')}}"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Account List</span></a>
                    </li>
                </ul>
            </li>
            {{--<li class=" nav-item"><a href="{{url('ListOfMembers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Members</span></a>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="{{url('ListOfMembers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Members</span></a>--}}
            {{--</li>--}}
            <li class=" nav-item"><a href="{{url('Addslider')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Home Slider</span></a>
            <li class=" nav-item"><a href="{{url('AddDestinations')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Safari Experience</span></a>
            <li class=" nav-item"><a href="{{url('AddAttractions')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Add Destination</span></a>
            <li class=" nav-item"><a href="{{url('AboutUsRow')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">About us</span></a>
            <li class=" nav-item"><a href="{{url('EnquireList')}}"><i class="fas fa-map-marker-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Enquire List</span></a>
            </li>

            <li class=" nav-item"><a href="#"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="">Services</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="{{url('FlightBookingRow')}}"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="nav.dash.main">Flight Booking</span></a></li>
                    <li class=" nav-item"><a href="{{url('SafariExperienceRow')}}"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="nav.dash.main">Safari Experience</span></a>
                    <li class=" nav-item"><a href="{{url('OurFleetRow')}}"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="nav.dash.main">Our Fleet</span></a>
                    </li>
                </ul>
            </li>

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="">Communication</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('SendEmail')}}"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="nav.dash.main">Send an Email</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('SendSMS')}}"><i class="fas fa-comment"></i><span class="menu-title" data-i18n="nav.dash.main">Send an SMS</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="#"><i class="fas fa-hotel"></i><span class="menu-title" data-i18n="nav.dash.main">Hotel Booking Requests</span></a>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="{{url('CompanyCategory')}}"><i class="fas fa-hotel"></i><span class="menu-title" data-i18n="nav.dash.main">Company Category</span></a>--}}
            {{--</li>--}}

        </ul>
    </div>
</div>


