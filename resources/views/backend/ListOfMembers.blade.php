
@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        #ui-datepicker-div{
            padding: 10px; table-responsive;
            background:#6b442b;
        }
        .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
            color: #fff !important;
            padding: 10px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>

    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                    <div class="row">

                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Type of Membership</th>
                                                <th>Chamber</th>
                                                <th>Company Name </th>
                                                <th>Company Code/Tin Number</th>
                                                <th>Hotel/Restaurant Category</th>
                                                <th>Number of Rooms</th>
                                                <th>Gender</th>
                                                <th>Phone Number</th>
                                                <th>P.O Box</th>
                                                <th>Email Address</th>
                                                <th>Website</th>
                                                <th>Business Address Street Name</th>
                                                <th>Building name</th>
                                                <th>Business Area(Quartier)</th>
                                                <th>Province</th>
                                                <th>District</th>
                                                <th>Sector</th>
                                                <th>Cell</th>
                                                <th>Company Type</th>
                                                <th>Ownership</th>
                                                <th>Business Activity</th>
                                                <th>Export/Import Goods/Services</th>
                                                <th>Number of Employees (Permanent)</th>
                                                <th>Number of Employees (Part time)</th>
                                                <th>RDB Certificate</th>
                                                <th>Approval</th>
                                                <th>Approved Date</th>
                                                <th>Expiration Date</th>
                                                <th>Identification</th>
                                                <th>Date Created</th>
                                                <th>Add Bank Slip</th>
                                                <th>Edit</th>
                                                <th>Last Edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listmembers as $data)
                                                <tr>
                                                    <td>{{$data->typeofmembership}}</td>
                                                    <td>{{$data->chamber}}</td>
                                                    <td>{{$data->companyname}}</td>
                                                    <td>{{$data->companycode}}</td>
                                                    <td>{{$data->hotelcategory}}</td>
                                                    <td>{{$data->numberofrooms}}</td>
                                                    <td>{{$data->gender}}</td>
                                                    <td>{{$data->phonenumber}}</td>
                                                    <td>{{$data->pobx}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->website}}</td>
                                                    <td>{{$data->businessaddress}}</td>
                                                    <td>{{$data->buildingname}}</td>
                                                    <td>{{$data->businessarea}}</td>
                                                    <td>{{$data->province}}</td>
                                                    <td>{{$data->district}}</td>
                                                    <td>{{$data->sector}}</td>
                                                    <td>{{$data->cell}}</td>
                                                    <td>{{$data->companytype}}</td>
                                                    <td>{{$data->ownership}}</td>
                                                    <td>{{$data->businessactivity}}</td>
                                                    <td>{{$data->export}}</td>
                                                    <td>{{$data->numberofemployees}}</td>
                                                    <td>{{$data->numberofemployeesapart}}</td>
                                                    <td>
                                                        <a href="rdbcertificate/{{$data->documentname}}" class="btn btn-secondary btn-min-width mr-1 mb-1" target="_blank"><i class="fas fa-file"></i> View Document</a>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <!-- Outline Icon Button group -->
                                                            <p style="text-align: center">{{$data->approval}}</p>
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Approved'])}}" class="btn btn-icon btn-outline-secondary"><i class="fas fa-user-check"></i></a>
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Denied'])}}" class="btn btn-icon btn-outline-primary"><i class="fas fa-user-times"></i></a>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td>{{$data->approved_date}}</td>
                                                    <td>{{$data->expiration_date}}</td>
                                                    <td>{{$data->identification}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                <?php
                                                                $bankslip =$data->bankslip;
                                                                $addbankslip = route('backend.AddBankSlip',['id'=> $data->id]);
                                                                if(empty($bankslip)){
                                                                    echo "<a href='$addbankslip' class='btn btn-icon btn-outline-secondary'><i class='fas fa-plus'></i></a>";
                                                                }else{
                                                                    echo "<a href='bankslip/$data->bankslip' class='btn btn-icon btn-outline-secondary'>View Bank Slip</a>";
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td><a href="{{ route('backend.EditJoinMember',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Edit</a></td>
                                                    <td>{{$data->name}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection
