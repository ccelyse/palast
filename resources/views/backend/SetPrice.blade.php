@extends('backend.layout.master')

@section('title', 'Palast')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #ff9027 !important;
            border-color: #ff9027 !important;
        }
        .btn-primary:hover{
            background-color: #ff9027 !important;
            border-color:#ff9027 !important;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Booking Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        @if(0 == count($getbookinfo))
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddBookingPrice') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6" >
                                                    <div class="form-group" hidden>
                                                        {{--<label for="projectinput1">Price </label>--}}
                                                        <input type="text" id="projectinput1" class="form-control"
                                                               name="booking_id" value="<?php echo  $id; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectinput1">Price </label>
                                                        <input type="text" id="projectinput1" class="form-control"
                                                               name="booking_price" value="{{ old('booking_price') }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Booking Invoice</label>
                                                        <input type="file" id="projectinput1" class="form-control"
                                                               name="booking_invoice" value="{{ old('booking_invoice') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <section id="basic" style="width: 100%;">
                                                    <div class="card">
                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <textarea class="summernote"  name="booking_moreinfo" id="booking_moreinfo"  required>{{ old('booking_moreinfo') }}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </section>
                                            </div>
                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                        </form>
                                            @else
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateBookingPrice') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                @foreach($getbookinfo as $datas)
                                                <div class="row">
                                                    <div class="col-md-6" >
                                                        <div class="form-group" hidden>
                                                            {{--<label for="projectinput1">Price </label>--}}
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="booking_id" value="{{$datas->booking_id}}">
                                                        </div>
                                                        <div class="form-group" hidden>
                                                            {{--<label for="projectinput1">Price </label>--}}
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="id" value="{{$datas->id}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="projectinput1">Price </label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="booking_price" value="{{$datas->booking_price}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Booking Invoice</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="booking_invoice">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <textarea class="summernote"  name="booking_moreinfo" id="booking_moreinfo" >{{$datas->booking_moreinfo}}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </section>
                                                </div>
                                                <a href="BookingInvoices/{{$datas->booking_invoice}}" class="btn btn-primary" target="_blank"> <i class="la la-check-square-o"></i> View booking invoice</a>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                @endforeach
                                            </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden="">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
