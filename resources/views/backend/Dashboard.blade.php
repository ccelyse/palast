@extends('backend.layout.master')

@section('title', 'Palast Tours')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script>
        $(document).ready(function () {
            var janmember = <?php echo $janmember; ?>;
            var febmember = <?php echo $febmember; ?>;
            var marmember = <?php echo $marmember; ?>;
            var aprmember = <?php echo $aprmember; ?>;
            var maymember = <?php echo $maymember; ?>;
            var junmember = <?php echo $junmember; ?>;
            var julmember = <?php echo $julmember; ?>;
            var augmember = <?php echo $augmember; ?>;
            var sepmember = <?php echo $sepmember; ?>;
            var octmember = <?php echo $octmember; ?>;
            var novmember = <?php echo $novmember; ?>;
            var decmember = <?php echo $decmember; ?>;
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'RHA Member Application\'s statistics 2018'
                },

                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: '\'Open Sans\',-apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,\'Helvetica Neue\',Arial,sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Applications'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<b>{point.y:.1f} Applications</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['Jan', janmember],
                        ['Feb', febmember],
                        ['Mar', marmember],
                        ['Apr', aprmember],
                        ['May', maymember],
                        ['Jun', junmember],
                        ['Jul', julmember],
                        ['Aug', augmember],
                        ['Sep', sepmember],
                        ['Oct', octmember],
                        ['Nov', novmember],
                        ['Dec', decmember]
                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: '\'Open Sans\',-apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,\'Helvetica Neue\',Arial,sans-serif'
                        }
                    }
                }]
            });
        });
    </script>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div id="crypto-stats-3" class="row">
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-5 pl-2">
                                        <h4>Applications</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $appmember;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-user-check blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-5 pl-2">
                                        <h4>Approved Members</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $approvedmember;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-user-times info font-large-1" title="XRP"></i></h1>
                                    </div>
                                    <div class="col-5 pl-2">
                                        <h4>Denied Members</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $deniedmember;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="xrp-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Active Orders -->
        </div>
    </div>
</div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection