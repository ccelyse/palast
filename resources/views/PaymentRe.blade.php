@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')

    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important; background-image:url('frontend/assets/images/Africa__0001_Africa-elephant-family-crossing-plains.jpg');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="frontend/assets/images/safariexperience.jpg" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">Payment Status</h1>
                            @if (session('success'))
                                <div class="alert alert-success" id="success_messages" style="margin-top: 50px;font-size: 35px;color: #fff;">
                                    {{ session('success') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('layouts.footer')
@endsection