@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')
<style>
    .eltd-tours-row.eltd-tours-columns-4 .eltd-tours-list-grid-sizer, .eltd-tours-row.eltd-tours-columns-4 .eltd-tours-row-item {
        width: 24% !important;
    }
</style>
    <div class="eltd-content" style="margin-top: -90px">
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="background-size: cover;height: 520px; background-position: top !important; background-image:url('frontend/assets/images/lakekivu2.jpg');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="frontend/assets/images/safariexperience.jpg" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">Discover our destination list.</h1>
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper">
                            <div class="eltd-row-grid-section">
                            <div class="eltd-row-grid-section-wrapper "  >
                                    <div class="eltd-row-grid-section">
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1520940084230" >
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="eltd-tours-destination-grid">
                                                            <div class="eltd-tours-destination-holder eltd-tours-row eltd-tours-columns-4 eltd-small-space">
                                                                <div class="eltd-tours-row-inner-holder eltd-outer-space">

                                                                    @foreach($listattractions as $destination)
                                                                        <div class="eltd-tours-destination-grid-item eltd-tours-row-item eltd-item-space post-230 destinations type-destinations status-publish has-post-thumbnail hentry">
                                                                            <div class="eltd-tours-destination-item-holder">
                                                                                <a href="{{ route('DestinationMore',['id'=> $destination->id])}}">
                                                                                    <div class="eltd-tours-destination-item-image">
                                                                                        <img width="800" height="790" src="attractions/{{$destination->attraction_image}}" class="attachment-full size-full wp-post-image" alt="a" srcset="attractions/{{$destination->attraction_image}}" sizes="(max-width: 800px) 100vw, 800px" />
                                                                                    </div>
                                                                                    <div class="eltd-tours-destination-item-content">
                                                                                        <div class="eltd-tours-destination-item-content-inner">
                                                                                            <h4 class="eltd-tours-destination-item-title">{{$destination->attraction_name}}</h4>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('layouts.footer')
@endsection