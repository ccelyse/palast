@extends('layouts.master')

@section('title', 'Palast Tours and Travel')

@section('content')
    @include('layouts.topmenu')

    <div class="eltd-content" style="margin-top: -90px">
        @foreach($listfleet as $data)
        <div class="eltd-content-inner">
            <div class="eltd-title-holder eltd-centered-type eltd-title-va-header-bottom eltd-preload-background eltd-has-bg-image eltd-bg-parallax" style="height: 520px;background-position: top !important; background-size: cover;background-image:url('OurFleet/{{$data->ourfleetimage}}');" data-height="520">
                <div class="eltd-title-image">
                    <img itemprop="image" src="OurFleet/{{$data->ourfleetimage}}" alt="a" />
                </div>
                <div class="eltd-title-wrapper" style="height: 520px">
                    <div class="eltd-title-inner">
                        <div class="eltd-grid">
                            <h1 class="eltd-page-title entry-title" style="color: #ffffff">{{$data->ourfleetcaption}}</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eltd-full-width">
            <div class="eltd-full-width-inner">
                <div class="eltd-grid-row">
                    <div class="eltd-page-content-holder eltd-grid-col-12">
                        <div class="eltd-row-grid-section-wrapper eltd-content-aligment-center">
                            <div class="eltd-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1519737413376">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="eltd-elements-holder   eltd-one-column  eltd-responsive-mode-768 ">
                                                    <div class="eltd-eh-item    " data-item-class="eltd-eh-custom-8750" data-1280-1600="65px 156px 81px" data-1024-1280="65px 156px 81px" data-768-1024="65px 0 81px" data-680-768="65px 0 81px" data-680="65px 0 81px">
                                                        <div class="eltd-eh-item-inner">
                                                            <div class="eltd-eh-item-content eltd-eh-custom-8750" style="padding: 65px 252px 81px">
                                                                <div class="eltd-section-title-holder   eltd-animated-separator">
                                                                    <div class="eltd-st-inner">
                                                                        <h2 class="eltd-st-title">
                                                                            {{$data->ourfleetcaption}}</h2>
                                                                        <div class="eltd-separator-holder clearfix  eltd-separator-center ">
                                                                            <div class="eltd-separator"></div>
                                                                        </div>

                                                                        <?php
                                                                        $text= $data->ourfleetmore;
                                                                        echo $text;
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <footer class="eltd-page-footer">
                                    <div class="eltd-footer-top-holder">
                                        <div class="eltd-footer-top-inner eltd-grid">
                                            <div class="eltd-grid-row eltd-footer-top-alignment-center eltd-footer-top-skin-dark">
                                                <div class="eltd-column-content eltd-grid-col-12">

                                                   <div id="eltd_instagram_widget-4" class="widget eltd-footer-column-1 widget_eltd_instagram_widget">
                                                        <ul class="eltd-instagram-feed clearfix eltd-col-8 eltd-instagram-carousel eltd-owl-slider" data-number-of-items="6" data-slider-margin="20" data-enable-navigation="yes" data-enable-pagination="no">
                                                           @foreach($gallery as $gallery)
                                                            <li>
                                                                    <img class="thumbnail" src="OurFleet/{{$gallery->fleetgallery}}" alt="" width="450" height="150" />
                                                            </li>
                                                            @endforeach
                                                            {{--<li>--}}
                                                                    {{--<img class="thumbnail" src="frontend/assets/images/IMG-20190201-WA0033.jpg" alt="" width="450" height="150" />--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                    {{--<img class="thumbnail" src="frontend/assets/images/IMG-20190201-WA0013.jpg" alt="" width="450" height="150" />--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                    {{--<img class="thumbnail" src="frontend/assets/images/IMG-20190201-WA0015.jpg" alt="" width="450" height="150" />--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                    {{--<img class="thumbnail" src="frontend/assets/images/IMG-20190201-WA0017.jpg" alt="" width="450" height="150" />--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                    {{--<img class="thumbnail" src="frontend/assets/images/IMG-20190201-WA0032.jpg" alt="" width="450" height="150" />--}}
                                                            {{--</li>--}}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        @endforeach
    </div>
    @include('layouts.footer')
@endsection