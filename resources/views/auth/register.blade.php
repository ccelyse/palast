
@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')
    <style>
        body.vertical-layout.vertical-menu.menu-expanded .content, body.vertical-layout.vertical-menu.menu-expanded .footer, body.vertical-layout.vertical-menu.menu-expanded .navbar .navbar-container {
            margin-left:0;
        }
        html body.vertical-layout {
            background: url(backend/app-assets/images/rad.jpg) center center no-repeat fixed;
            -webkit-background-size: cover;
            background-size: cover;
        }
        body.vertical-layout.vertical-menu.menu-expanded .content, body.vertical-layout.vertical-menu.menu-expanded .footer, body.vertical-layout.vertical-menu.menu-expanded .navbar .navbar-container {
            margin-left: 0px !important;
        }
        .btn-login {
            border-color: #6b442b !important;
            background-color: #6b442b !important;
            color: #FFF;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/login-register.min.css">


    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <div class="p-1">
                                            <img src="backend/app-assets/images/RHAlogo.png" alt="branding logo" style="width: 100px;">
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                        <span>Create Account</span>
                                    </h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                       <form class="form-horizontal form-simple" method="POST" action="{{ route('register') }}">
                                                {{ csrf_field() }}
                                            <fieldset class="form-group{{ $errors->has('name') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
                                                <input type="text" class="form-control form-control-lg input-lg" id="user-name" name="name" value="{{ old('name') }}"  placeholder="names" autofocus>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                         <strong>{{ $errors->first('name') }}</strong>
                                                     </span>
                                                @endif
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group{{ $errors->has('email') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
                                                <input type="email" class="form-control form-control-lg input-lg" id="user-email" name="email" name="email" value="{{ old('email') }}"  placeholder="email" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                         <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }} position-relative has-icon-left">
                                                <input type="password" class="form-control form-control-lg input-lg" id="user-password"
                                                       placeholder="Enter Password" name="password"  required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                         <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif

                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>

                                           <fieldset class="form-group position-relative has-icon-left">
                                               <input type="password" class="form-control form-control-lg input-lg" id="user-password"
                                                      placeholder="Confirm password" name="password_confirmation"  required>
                                               <div class="form-control-position">
                                                   <i class="la la-key"></i>
                                               </div>
                                           </fieldset>
                                            <button type="submit" class="btn btn-info btn-lg btn-block"><i class="ft-unlock"></i> Register</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection