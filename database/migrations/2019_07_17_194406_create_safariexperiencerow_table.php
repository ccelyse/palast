<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafariexperiencerowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safariexperiencerow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('safariexperiencecaption');
            $table->string('safariexperienceimage');
            $table->longText('safariexperiencemore');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safariexperiencerow');
    }
}
