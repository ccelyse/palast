<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PaymentTransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enquire_code');
            $table->string('vpc_Amount')->nullable();
            $table->string('vpc_BatchNo')->nullable();
            $table->string('vpc_Card')->nullable();
            $table->string('vpc_CardNum')->nullable();
            $table->string('vpc_Currency')->nullable();
            $table->string('vpc_MerchTxnRef')->nullable();
            $table->string('vpc_Merchant')->nullable();
            $table->string('vpc_Message')->nullable();
            $table->string('vpc_OrderInfo');
            $table->string('vpc_ReceiptNo')->nullable();
            $table->string('vpc_SecureHash')->nullable();
            $table->string('vpc_SecureHashType')->nullable();
            $table->string('vpc_TransactionNo')->nullable();
            $table->string('vpc_VerStatus')->nullable();
            $table->string('vpc_VerToken')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PaymentTransactions');
    }
}
