<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquireNowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquire_now', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('destination_id');
            $table->string('enquire_title');
            $table->string('enquire_first_name');
            $table->string('enquire_last_time');
            $table->string('enquire_telephone');
            $table->string('enquire_email');
            $table->string('enquire_country');
            $table->string('enquire_subject');
            $table->longText('enquire_message');
            $table->string('enquire_code');

            $table->foreign('destination_id')->references('id')->on('Attractions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquire_now');
    }
}
