<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightbookingrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flightbookingrow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flightbookingcaption');
            $table->string('flightbookingimage');
            $table->string('flightbookingmore');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flightbookingrow');
    }
}
