<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetBookingPrice extends Model
{
    protected $table = "set_booking_prices";
    protected $fillable = ['id','booking_id','booking_price','booking_invoice','booking_moreinfo'];
}
