<?php

namespace App\Http\Controllers;

use App\AboutUsRow;
use App\ActivityDetails;
use App\ActivitySummary;
use App\Attractions;
use App\Countries;
use App\EnquireNow;
use App\FleetGallery;
use App\FlightBookingRow;
use App\HomeSlider;
use App\JoinMember;
use App\OurFleetRow;
use App\PaymentTransactions;
use App\Post;
use App\SafariExperienceRow;
use App\SetBookingPrice;
use Carbon\Carbon;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
require ('bk-payment/function.php');
class FrontendController extends Controller
{

    public function AddDomainTest(Request $request){
        $all = $request->all();
        $certpath = \File::get(public_path() . '/test.pem');
//        $certpath=dirname(__FILE__) .'/test.pem';
        $params=array("Username" => "skydig_rwf","Password"=>"Attack@2017_!","Server"=>"registry2.ricta.org.rw","Port"=>"700","Certificate"=>$certpath,"SSL"=>"on");
        try {
            $client = $this->epp_Client($params);

        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

//Domain Check
        $request =  $client->request($xml ='
               <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
                 <command>
                   <check>
                     <domain:check xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
                       <domain:name>sd.rw</domain:name>
                     </domain:check>
                   </check>
                   <clTRID>ABC-12345</clTRID>
                 </command>
               </epp>
        ');

        # Parse XML result
        $doc= new DOMDocument();
        $doc->loadXML($request);


        $coderes = $doc->getElementsByTagName('result')->item(0)->getAttribute('code');
        $msg = $doc->getElementsByTagName('msg')->item(0)->nodeValue;

        # Check results
        if($coderes != '1000') {

            echo "Code (".$coderes.") ".$msg;
        }
        print_r($doc->saveXML());
    }


    public function epp_Client($params) {
        # Setup include dir


        # Include EPP stuff we need
        require_once('./Net/EPP/Client.php');
        require_once('./Net/EPP/Protocol.php');
//        require_once dirname(__FILE__) . '/Net/EPP/Client.php';
//        require_once dirname(__FILE__) .'/Net/EPP/Protocol.php';


        # Grab module parameters


        # Are we using ssl?
        $use_ssl = false;
        if (isset($params['SSL']) && $params['SSL'] == 'on') {
            $use_ssl = true;
        }

        # Set certificate if we have one
        if ($use_ssl && !empty($params['Certificate'])) {
            if (!file_exists($params['Certificate'])) {
                return ("Certificate file does not exist");
            }

            # Create SSL context
            //$context = stream_context_create();
            $context = stream_context_create(array(
                    'ssl' => array(
                        'verify_peer'      => false,
                        'verify_peer_name' => false,
                    ),
                )
            );

            stream_context_set_option($context, 'ssl', 'local_cert', $params['Certificate']);
        }
        try {
            # Create EPP client
            $client = new Net_EPP_Client();
            # Connect
            $res = $client->connect($params['Server'], $params['Port'], 60, $use_ssl, $context);

        }catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        # Perform login
        $request = $client->request($xml = '
        <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
            <command>
                <login>
                    <clID>'.$params['Username'].'</clID>
                    <pw>'.$params['Password'].'</pw>
                    <options>
                    <version>1.0</version>
                    <lang>en</lang>
                    </options>
                    <svcs>
                        <objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
                        <objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
                    </svcs>
                </login>
            </command>
        </epp>
        ');

        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($request);
        file_put_contents(dirname(__FILE__) .'./debug/login-request.xml', $xml, FILE_APPEND);
        file_put_contents(dirname(__FILE__) .'./debug/login-response.xml', $doc->saveXML(), FILE_APPEND);

        return $client;
    }
    public function Home(){
        $listslider = HomeSlider::orderBy('id','desc')->limit('5')->get();
        $listattractions = ActivitySummary::all();
//        $listattractions = Attractions::all();
        return view('welcome')->with(['listattractions' => $listattractions,'listslider'=>$listslider]);
    }

    public function FlightBooking(){
        $listflight = FlightBookingRow::all();
        return view('FlightBooking')->with(['listflight'=>$listflight]);
    }
    public function SafariExperiences(){
        $listattractions = Attractions::all();
        $listdestination = ActivitySummary::all();
        $listsafari = SafariExperienceRow::all();
        return view('SafariExperiences')->with(['listsafari'=>$listsafari,'listdestination'=>$listdestination,'listattractions' => $listattractions]);
//        return view('SafariExperiences');
    }
    public function ActivityMore(Request $request){
        $all = $request->all();
        $destinationid = $request['id'];
        $listmore = ActivitySummary::where('id',$destinationid)->get();
//        dd($listmore);
        $getdetails = ActivityDetails::where('activitysummary_id',$destinationid)->get();
//        $listcountries = Countries::all();
        return view('ActivityMore')->with(['getdetails'=>$getdetails,'listmore'=>$listmore]);

    }
    public function PalastAssist(){
        return view('PalastAssist');
    }
    public function OurFleet(){
        $listfleet = OurFleetRow::all();
        $gallery = FleetGallery::all();
        return view('OurFleet')->with(['listfleet'=>$listfleet,'gallery'=>$gallery]);
    }
    public function AboutUs(){
        $listabout = AboutUsRow::all();
        return view('Aboutus')->with(['listabout'=>$listabout]);
    }
    public function Destinations(){
        $listattractions = Attractions::all();
        return view('Destinations')->with(['listattractions' => $listattractions]);
    }
    // public function Destinations(){
    //     return view('Destinations');
    // }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
    public function DomainTest(){
        return view('backend.DomainTest');
    }
    public function Enquire(Request $request){
        $id = $request['id'];
        $list_countries = Countries::all();
        return view('Enquire')->with(['id'=>$id,'list_countries'=>$list_countries]);
    }
    public function EnquireNow(Request $request){
        $all = $request->all();
        $add = new EnquireNow();
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99). $currentMonth;

        $add->enquire_title = $request['enquire_title'];
        $add->destination_id = $request['destination_id'];
        $add->enquire_first_name = $request['enquire_first_name'];
        $add->enquire_last_time = $request['enquire_last_time'];
        $add->enquire_telephone = $request['enquire_telephone'];
        $add->enquire_email = $request['enquire_email'];
        $add->enquire_country = $request['enquire_country'];
        $add->enquire_subject = $request['enquire_subject'];
        $add->enquire_message = $request['enquire_message'];
        $add->enquire_code = $code;
        $add->save();

        $Fname = $request['enquire_first_name'];
        $Lname = $request['enquire_last_time'];
        $email = $request['enquire_email'];
        $message = $request['enquire_message'];
        $subjectmail = $request['enquire_subject'];


        Mail::send('mail',
            array(
                'namemail'=>'Palast Tours',
                'name' => $Fname,
                'email' => $email,
                'messagemail' => $message,
                'Fname' => $Fname,
                'Lname' => $Lname,
            ), function($message) use ($email, $Fname,$subjectmail)
            {
                $message->from($email, $Fname);
                $message->to('kalisa913@gmail.com', 'Palast Tours')->subject($subjectmail);
            });

        return back()->with('success','you successfully sent an enquiry, we shall get back to in less than 24 hours');
    }

    public function PayNow(Request $request){
        $id = $request['id'];
//        dd($id);
//        $id = '1';
        $getinfo = SetBookingPrice::where('booking_id',$id)->join('enquire_now', 'enquire_now.id', '=', 'set_booking_prices.booking_id')
                ->select('enquire_now.destination_id','enquire_now.enquire_title','enquire_now.enquire_first_name',
                    'enquire_now.enquire_title','enquire_now.enquire_title','enquire_now.enquire_title',
                    'enquire_now.enquire_last_time','enquire_now.enquire_telephone','enquire_now.enquire_email',
                    'enquire_now.enquire_country','set_booking_prices.*')
                ->get();
        return view('PayNow')->with(['id'=>$id,'getinfo'=>$getinfo,]);
    }
    public function MakePayment(Request $request){
        $all = $request->all();
        $booking_price = $request['booking_price'];
        $newamount = ltrim($booking_price, '$');
        $amount = $newamount;
        $currency = 'USD';
        //'RWF';
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99999). $currentMonth;
        $orderInfo = $code;

        $getenquriecode = EnquireNow::where('id',$request['booking_id'])->value('enquire_code');
        $transaction = new PaymentTransactions();
        $transaction->vpc_Amount = $amount;
        $transaction->enquire_code = $getenquriecode;
        $transaction->vpc_OrderInfo = $orderInfo;
        $transaction->save();

        /**
         * Our merchant account configurations.. How to acquire those?
         *
         * This data will be encrypted and saved securely in our database..
         */

        // active
        $accountData = array(
            'merchant_id' => env('MERCHANT'),
            'access_code' => env('ACCESS_CODE'),
            'secret'      => env('SECRET'),
        );
// multi currency

        if($currency=="RWF"){
            // $_PDT['vpc_Currency']=646;
            $mult = 1;
        }
        elseif($currency=="USD"){
            // $_PDT['vpc_Currency']=840;
            $mult = 100;
        }

        /**
         * Query data..
         */
        $queryData = array(
            'vpc_AccessCode' => $accountData['access_code'],
            'vpc_Merchant' => $accountData['merchant_id'],

            'vpc_Amount' => ($amount * $mult), // Multiplying by 100 to convert to the smallest unit
            'vpc_OrderInfo' => $orderInfo,

            'vpc_MerchTxnRef' => generateMerchTxnRef(), // See functions.php file

            'vpc_Command' => 'pay',
            'vpc_Currency' => $currency,
            'vpc_Locale' => 'en',
            'vpc_Version' => 1,

//            'vpc_ReturnURL' => 'https://akokanya.com/payment-callback',
            'vpc_ReturnURL' => 'http://127.0.0.1:9024/payment-callback',
            'vpc_SecureHashType' => 'SHA256'
        );

// Add secure secret after hashing
        $queryData['vpc_SecureHash'] = generateSecureHash($accountData['secret'], $queryData); // See functions.php file
        $migsUrl = 'https://migs.mastercard.com.au/vpcpay?'.http_build_query($queryData);
        $migsUrlProduction = 'https://migs-mtf.mastercard.com.au/vpcpay?'.http_build_query($queryData);

        return \Illuminate\Support\Facades\Redirect::to($migsUrlProduction);
        header("Location: " . $migsUrlProduction);
    }
    public function PaymentCallback(Request $request){
        $all = $request->all();
        $Transaction = PaymentTransactions::where('vpc_OrderInfo',$request['vpc_OrderInfo'])
            ->update([
                'vpc_BatchNo'=> $request['vpc_BatchNo'],
                'vpc_Card'=> $request['vpc_Card'],
                'vpc_CardNum'=> $request['vpc_CardNum'],
                'vpc_Currency'=> $request['vpc_Currency'],
                'vpc_MerchTxnRef'=> $request['vpc_MerchTxnRef'],
                'vpc_Merchant'=> $request['vpc_Merchant'],
                'vpc_Message'=> $request['vpc_Message'],
                'vpc_OrderInfo'=> $request['vpc_OrderInfo'],
                'vpc_ReceiptNo'=> $request['vpc_ReceiptNo'],
                'vpc_SecureHash'=> $request['vpc_SecureHash'],
                'vpc_SecureHashType'=> $request['vpc_SecureHashType'],
                'vpc_TransactionNo'=> $request['vpc_TransactionNo'],
                'vpc_VerStatus'=> $request['vpc_VerStatus'],
                'vpc_VerToken'=> $request['vpc_VerToken'],
            ]);
        $newamount = $request['vpc_Amount'] / 100;
        $message = "We have successfully received your payment of the amount of $$newamount";

        if($request['vpc_Message'] == 'Approved'){
            return redirect()->route('PaymentRe')->with('success',$message);
        }else{
            return redirect()->route('PaymentRe')->with('success','Your Transaction has failed , kindly try again');
        }
    }
    public function PaymentRe(){
        return view('PaymentRe');
    }
    public function Receipt(){
        $code = '3182210';

        $getenquirecode = PaymentTransactions::where('vpc_OrderInfo',$code)->value('enquire_code');
        $vpc_Amount = PaymentTransactions::where('vpc_OrderInfo',$code)->value('vpc_Amount');
        $getinfo = PaymentTransactions::where('vpc_OrderInfo',$code)->get();
        $getnames = EnquireNow::where('enquire_code',$getenquirecode)->get();
        $getdestinationid = EnquireNow::where('enquire_code',$getenquirecode)->value('destination_id');
        $getdestinationinfo = Attractions::where('id',$getdestinationid)->value('attraction_name');

        return view('Receipt')->with(['getinfo'=>$getinfo,'getnames'=>$getnames,'vpc_Amount'=>$vpc_Amount,'getdestinationinfo'=>$getdestinationinfo]);
    }
    public function AttractionAPI(){
        $listattractions = Attractions::select('id','attraction_province','attraction_name','attraction_image')->get();
        return response()->json($listattractions);
    }
    public function MoreAttractionAPI(Request $request){
        $id = $request['id'];
        $listattractions = Attractions::where('id',$id)->get();
        return response()->json($listattractions);
    }

    public function AttractionsMore(Request $request){
        $all = $request->all();
        $attraction_id = $request['id'];
        $listmore = Attractions::where('id',$attraction_id)->get();
        $listcountries = Countries::all();
        return view('AttractionsMore')->with(['listmore'=>$listmore,'listcountries'=>$listcountries]);

    }

    public function SendingEmail(Request $request) {
        $all = $request->all();
//        dd($all);

        $name = $request['names'];

        $email = $request['email'];
        $message = $request['message'];
        $subjectmail = $request['subject'];

        $data = array(
            'namemail'=>'Palast Tours',
            'messagemail'=>$message,
        );


        Mail::send('mail',
            array(
                'namemail'=>'Palast Tours',
                'name' => $name,
                'email' => $email,
                'messagemail' => $message
            ), function($message) use ($email, $name,$subjectmail)
            {
                $message->from($email, $name);
                $message->to('info@palasttours.rw', 'Palast Tours')->subject($subjectmail);
            });

        return view('ThankYou');
    }

    public function ThankYou(){
        return view('ThankYou');
    }

}
