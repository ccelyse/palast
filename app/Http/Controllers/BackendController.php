<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\AboutUsRow;
use App\ActivityDetails;
use App\ActivitySummary;
use App\Attractions;
use App\CompanyCategory;
use App\Countries;
use App\EnquireNow;
use App\FleetGallery;
use App\FlightBookingRow;
use App\Hireaguide;
use App\HomeSlider;
use App\JoinMember;
use App\OurFleetRow;
use App\Post;
use App\SafariExperienceRow;
use App\SetBookingPrice;
use App\User;
use Carbon\Carbon;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BackendController extends Controller
{
    public function SignIn_(){

    }
    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        /*$now = Carbon::now();
        $nowdate = Carbon::now();
        $nowyear = $now->addYear(2);


        $day = strtotime($now->toDateString());

        $renewdate = JoinMember::whereBetween('expiration_date',[$nowdate,$nowyear])->get();

        foreach ($renewdate as $datas){
            $date_renew = $datas->expiration_date;
            $dateonly = strtotime($date_renew);
//            echo "<p>$dateonly</p>";
            $nowexp = time();
            $datediff = $dateonly - $nowexp;
            $daysexpiration = round($datediff / (60 * 60 * 24));

            if ($daysexpiration >= 30){

            }
        }*/


        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         *  application member
         */

        $appmember = JoinMember::select(DB::raw('count(id) as appmember'))->value('appmember');

        $approvedmember = JoinMember::select(DB::raw('count(id) as approvedmember'))
            ->where('approval','Approved')
            ->value('approvedmember');

        $deniedmember = JoinMember::select(DB::raw('count(id) as deniedmember'))
            ->where('approval','Denied')
            ->value('deniedmember');

        $guiderequest = Hireaguide::select(DB::raw('count(id) as guiderequest'))->value('guiderequest');

        /**
         *
         * MEmber application  (JANUARY)
         */

        $janmember = JoinMember::select(DB::raw('count(id) as janmember'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('janmember');

        /**
         *
         * MEmber application  (Feb)
         */

        $febmember = JoinMember::select(DB::raw('count(id) as febmember'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('febmember');

        /**
         *
         * MEmber application  (mar)
         */

        $marmember = JoinMember::select(DB::raw('count(id) as marmember'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('marmember');
        /**
         *
         * MEmber application  (apr)
         */

        $aprmember = JoinMember::select(DB::raw('count(id) as aprmember'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('aprmember');

        /**
         *
         * MEmber application  (may)
         */

        $maymember = JoinMember::select(DB::raw('count(id) as maymember'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('maymember');

        /**
         *
         * MEmber application  (jun)
         */

        $junmember = JoinMember::select(DB::raw('count(id) as junmember'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('junmember');

        /**
         *
         * MEmber application  (jul)
         */

        $julmember = JoinMember::select(DB::raw('count(id) as julmember'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('julmember');

        /**
         *
         * MEmber application  (aug)
         */

        $augmember = JoinMember::select(DB::raw('count(id) as augmember'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('augmember');

        /**
         *
         * MEmber application  (sep)
         */

        $sepmember = JoinMember::select(DB::raw('count(id) as sepmember'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('sepmember');


        /**
         *
         * MEmber application  (oct)
         */


        $octmember = JoinMember::select(DB::raw('count(id) as octmember'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('octmember');

        /**
         *
         * MEmber application  (nov)
         */


        $novmember = JoinMember::select(DB::raw('count(id) as novmember'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('novmember');

        /**
         *
         * MEmber application  (dec)
         */

        $decmember = JoinMember::select(DB::raw('count(id) as decmember'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('decmember');

        return view('backend.Dashboard')->with(['janmember'=>$janmember,'febmember'=>$febmember,
            'marmember'=>$marmember,'aprmember'=>$aprmember,'maymember'=>$maymember,'junmember'=>$junmember,'julmember'=>$julmember,
            'augmember'=>$augmember,'sepmember'=>$sepmember,'octmember'=>$octmember,'novmember'=>$novmember,'decmember'=>$decmember,
            'appmember'=>$appmember,'approvedmember'=>$approvedmember,'deniedmember'=>$deniedmember,'guiderequest'=>$guiderequest]);
    }
    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function ListOfMembers(){
        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.ListOfMembers')->with(['listmembers'=>$listmembers]);

    }
    public function FilterMembers(){

        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.FilterMembers')->with(['listmembers'=>$listmembers]);
    }
    public function FilterMembers_(Request $request){
        $all = $request->all();
        $district = $request['district'];
        $filter = JoinMember::where('district', 'LIKE', '%'.$district.'%')->get();
        return view('backend.FilterMembers')->with(['filter'=>$filter]);

    }

    public function ApproveMember(Request $request){

        $id = $request['MemberId'];
        $status = $request['Status'];
        $currentMonth = Carbon::now()->format('m');
        $code = 'RHA'. mt_rand(10, 99). $currentMonth;
        $date_now = Carbon::now();
        $dateafteryear = Carbon::now()->addYear();


        switch ($status) {
            case "Approved":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Approved';
                $update_member->Memberstatus ='Approved';
                $update_member->approved_date =$date_now;
                $update_member->expiration_date =$dateafteryear;
                $update_member->identification =$code;
                $update_member->save();
                break;

            case "Denied":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Denied';
                $update_member->Memberstatus ='Denied';
                $update_member->identification ='';
                $update_member->approved_date =NULL;
                $update_member->expiration_date =NULL;
                $update_member->save();
                break;

        }
        return back()->with('success','You have successfully updated members list');
    }
    public function AddAttractions(){
        $listattractions = Attractions::all();
        return view('backend.AddAttractions')->with(['listattractions'=>$listattractions]);
    }
    public function EditJoinMember(Request $request){
        $id = $request['id'];

        $edit = JoinMember::where('id',$id)->get();

        return view('backend.EditJoinMember')->with(['edit'=>$edit]);
    }

    public function AddAttractions_(Request $request){
        $all = $request->all();
//        dd($all);
//        $request->validate([
//            'fileToUpload'   => 'mimes:pdf'
//        ]);

        $saveattractions = new Attractions();

        $image = $request->file('fileToUpload');

        $saveattractions->attraction_province = $request['attraction_province'];
        $saveattractions->attraction_name = $request['attraction_name'];
        $saveattractions->attraction_indetails = $request['attraction_indetails'];

        $saveattractions->attraction_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/attractions');
        $image->move($destinationPath, $imagename);

        $saveattractions->save();

        return back()->with('success','you have created a new attraction');
    }
    public function EditAttractions(Request $request){
        $id = $request['id'];
        $image = $request->file('fileToUpload');

        if($request->file('fileToUpload')){
            $editdest = Attractions::find($id);
            $editdest->attraction_province = $request['attraction_province'];
            $editdest->attraction_name = $request['attraction_name'];
            $editdest->attraction_indetails = $request['attraction_indetails'];

            $editdest->attraction_image = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/attractions');
            $image->move($destinationPath, $imagename);
            $editdest->save();

            return back()->with('success','you have edited attraction');

        }else{

            $editdest = Attractions::find($id);
            $editdest->attraction_province = $request['attraction_province'];
            $editdest->attraction_name = $request['attraction_name'];
            $editdest->attraction_indetails = $request['attraction_indetails'];
            $editdest->save();

            return back()->with('success','you have edited attraction');
        }
    }
    public function DeleteAttraction(Request $request){
        $id = $request['id'];
        $deleteattr = Attractions::find($id);
        $deleteattr->delete();
        return back()->with('success','you have deleted attraction');
    }

    public function VisitRwanda()
    {
        $listattractions = Attractions::all();
        return view('VisitRwanda')->with(['listattractions' => $listattractions]);
    }
    public function DestinationMore(Request $request){
        $all = $request->all();
        $attraction_id = $request['id'];
        $listmore = Attractions::where('id',$attraction_id)->get();
        $listcountries = Countries::all();
        return view('DestinationMore')->with(['listmore'=>$listmore,'listcountries'=>$listcountries]);

    }
    public function SendSMS(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendSMS')->with(['listnumber'=>$listnumber]);
    }
    public function SendSMS_(Request $request){
//        $all = $request->all();
//        dd($all);


//        $username   = "rha";
//        $apiKey     = "55fa2436c4624ed856c4910111fde3ecd425a6bc738ed2fc20d6c85c438258b8";
        $message = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $pluscode = '+25';
        $recipients = $pluscode . $phonenumber;

        $username = 'sandbox'; // use 'sandbox' for development in the test environment
        $apiKey 	= 'fa1c39e0943a7802136e51e9b04acc43367003b517b09e6954463a688c27315f'; // use your sandbox app API key for development in the test environment
        $AT = new AfricasTalking($username, $apiKey,"sandbox");
        $SMS = $AT->sms();
        $options = array(
            "to" => '+250782384772',
            "message" => "$message",
        );

        $SMS->send($options);
        return redirect()->route('backend.SendSMS')->with('success','you have successfully sent your message');
    }
    public function SendEmail(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendEmail')->with(['listnumber'=>$listnumber]);
    }
    public function SendEmail_(Request $request){
        $all = $request->all();
        dd($all);
    }
    public function CompanyCategory(){
        $listcomp = CompanyCategory::all();
        return view('backend.CompanyCategory')->with(['listcomp'=>$listcomp]);
    }
    public function CompanyCategory_(Request $request){
        $all = $request->all();

        $addcompanycategory = new CompanyCategory();
        $addcompanycategory->company_category = $request['companycategory'];
        $addcompanycategory->save();

        return back()->with('success','you successfully added a new company category');

    }
    public function EditCompanyCategory(Request $request){
        $id = $request['id'];
        $editcomp = CompanyCategory::where('id',$id)->get();
        return view('backend.EditCompanyCategory')->with(['editcomp'=>$editcomp]);
    }
    public function EditCompanyCategory_(Request $request){
        $id  = $request['id'];
        $updatecomp = CompanyCategory::find($id);
        $updatecomp->company_category = $request['companycategory'];
        $updatecomp->save();

        return redirect()->route('backend.CompanyCategory')->with('success','You have successful company category');
    }
    public function DeleteCompanycategory(Request $request){
        $id = $request['id'];
        $deletecomp = CompanyCategory::find($id);
        $deletecomp->delete();
        return back();
    }
    public function Addslider(){
        $slider = HomeSlider::all();
        return view('backend.Addslider')->with(['slider'=>$slider]);
    }
    public function AddHomeslider(Request $request){
        $homeslider = new HomeSlider();
        $homeslider->slidercaption = $request['slidercaption'];

        $image = $request->file('sliderimage');
        $homeslider->sliderimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/SliderImage');
        $image->move($destinationPath, $imagename);
        $homeslider->save();
        return back()->with('success','you successfully added a new Homeslider');
    }
    public function EditHomeslider(Request $request){
        $id = $request['id'];
        $editslider = HomeSlider::find($id);

        if($request->file('sliderimage')){
            $editslider->slidercaption = $request['slidercaption'];
            $image = $request->file('sliderimage');
            $editslider->sliderimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/SliderImage');
            $image->move($destinationPath, $imagename);
            $editslider->save();
            return back()->with('success','you successfully edited  Homeslider');
        }else{
            $editslider->slidercaption = $request['slidercaption'];
            $editslider->save();
            return back()->with('success','you successfully edited Homeslider');
        }

    }
    public function DeleteHomeslider(Request $request){
        $id = $request['id'];
        $delete = HomeSlider::find($id);
        $delete->delete();
        return back()->with('success','you successfully deleted Homeslider');
    }

    public function AboutUsRow(){
//        $listactivity = ActivitySummary::all();
        $listattractions = Attractions::all();
        $listabout = AboutUsRow::all();
        return view('backend.AboutUsRow')->with(['listabout'=>$listabout]);
    }
    public function AddAboutUsRow(Request $request){
        $homeslider = new AboutUsRow();
        $homeslider->aboutuscaption = $request['aboutuscaption'];
        $homeslider->aboutusmore = $request['aboutusmore'];

        $image = $request->file('aboutusimage');
        $homeslider->aboutusimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AboutUs');
        $image->move($destinationPath, $imagename);
        $homeslider->save();
        return back()->with('success','you successfully added a new AboutUs Info');
    }
    public function EditAboutUsRow(Request $request){
        $id = $request['id'];
        $editabout = AboutUsRow::find($id);

        if($request->file('aboutusimage')){
            $editabout->aboutuscaption = $request['aboutuscaption'];
            $editabout->aboutusmore = $request['aboutusmore'];
            $image = $request->file('aboutusimage');
            $editabout->aboutusimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AboutUs');
            $image->move($destinationPath, $imagename);
            $editabout->save();
            return back()->with('success','you successfully edited a new AboutUs Info');
        }else{
            $editabout->aboutuscaption = $request['aboutuscaption'];
            $editabout->aboutusmore = $request['aboutusmore'];
            $editabout->save();
            return back()->with('success','you successfully edited a new AboutUs Info');
        }
    }
    public function FlightBookingRow(){
        $listflight = FlightBookingRow::all();
        return view('backend.FlightBookingRow')->with(['listflight'=>$listflight]);
    }
    public function AddFlightBookingRow(Request $request){
        $flightbooking = new FlightBookingRow();
        $flightbooking->flightbookingcaption = $request['flightbookingcaption'];
        $flightbooking->flightbookingmore = $request['flightbookingmore'];

        $image = $request->file('flightbookingimage');
        $flightbooking->flightbookingimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/FlightBooking');
        $image->move($destinationPath, $imagename);
        $flightbooking->save();
        return back()->with('success','you successfully added a new Flight Booking Info');
    }
    public function EditFlightBookingRow(Request $request){
        $id = $request['id'];
        $editflight = FlightBookingRow::find($id);
        if($request->file('flightbookingimage')){

            $editflight->flightbookingcaption = $request['flightbookingcaption'];
            $editflight->flightbookingmore = $request['flightbookingmore'];

            $image = $request->file('flightbookingimage');
            $editflight->flightbookingimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/FlightBooking');
            $image->move($destinationPath, $imagename);
            $editflight->save();

            return back()->with('success','you successfully edited Flight Booking Info');
        }else{

            $editflight->flightbookingcaption = $request['flightbookingcaption'];
            $editflight->flightbookingmore = $request['flightbookingmore'];
            $editflight->save();
            return back()->with('success','you successfully edited Flight Booking Info');
        }
    }
    public function SafariExperienceRow(){
        $listsafari = SafariExperienceRow::all();
        return view('backend.SafariExperienceRow')->with(['listsafari'=>$listsafari]);
    }
    public function AddSafariExperience(Request $request){
        $safari = new SafariExperienceRow();
        $safari->safariexperiencecaption = $request['safariexperiencecaption'];
        $safari->safariexperiencemore = $request['safariexperiencemore'];

        $image = $request->file('safariexperienceimage');
        $safari->safariexperienceimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/SafariExperience');
        $image->move($destinationPath, $imagename);
        $safari->save();
        return back()->with('success','you successfully added a new Safari Experience Info');
    }
    public function EditSafariExperienceRow(Request $request){
        $id = $request['id'];
        $safari = SafariExperienceRow::find($id);
        if($request->file('safariexperienceimage')){
            $safari->safariexperiencecaption = $request['safariexperiencecaption'];
            $safari->safariexperiencemore = $request['safariexperiencemore'];

            $image = $request->file('safariexperienceimage');
            $safari->safariexperienceimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/SafariExperience');
            $image->move($destinationPath, $imagename);
            $safari->save();
            return back()->with('success','you successfully added a new Safari Experience Info');
        }else{
            $safari->safariexperiencecaption = $request['safariexperiencecaption'];
            $safari->safariexperiencemore = $request['safariexperiencemore'];
            $safari->save();
            return back()->with('success','you successfully added a new Safari Experience Info');
        }
    }
    public function OurFleetRow(){
        $listsafari = OurFleetRow::all();
        $listgallery = FleetGallery::all();
        return view('backend.OurFleetRow')->with(['listsafari'=>$listsafari,'listgallery'=>$listgallery]);
    }
    public function DeleteOurFleetRow(Request $request){
        $id = $request['id'];
        $delete = FleetGallery::find($id);
        $delete->delete();
        return back()->with('success','you successfully delete Our Fleet Gallery Info');
    }
    public function AddOurFleetRow(Request $request){
        $fleet = new OurFleetRow();
        $fleet->ourfleetcaption = $request['ourfleetcaption'];
        $fleet->ourfleetmore = $request['ourfleetmore'];

        $image = $request->file('ourfleetimage');
        $fleet->ourfleetimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/OurFleet');
        $image->move($destinationPath, $imagename);
        $fleet->save();
        return back()->with('success','you successfully added a new Our Fleet Info');
    }
    public function EditOurFleetRow(Request $request){
        $id = $request['id'];
        $fleet = OurFleetRow::find($id);
        if($request->file('ourfleetimage')){
            $fleet->ourfleetcaption = $request['ourfleetcaption'];
            $fleet->ourfleetmore = $request['ourfleetmore'];

            $image = $request->file('ourfleetimage');
            $fleet->ourfleetimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/OurFleet');
            $image->move($destinationPath, $imagename);
            $fleet->save();
            return back()->with('success','you successfully edited Our Fleet Info');
        }else{
            $fleet->ourfleetcaption = $request['ourfleetcaption'];
            $fleet->ourfleetmore = $request['ourfleetmore'];
            $fleet->save();
            return back()->with('success','you successfully edited Our Fleet Info');
        }
    }

    public function AddFleetGallery(Request $request){
        $fleetgallery = new FleetGallery();
        $image = $request->file('fleetgallery');
        $fleetgallery->fleetgallery = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/OurFleet');
        $image->move($destinationPath, $imagename);
        $fleetgallery->save();
        return back()->with('success','you successfully added Our Fleet Gallery Info');
    }

    public function AddDestinations(){
        $listactivity = ActivitySummary::all();
        return view('backend.AddDestinations')->with(['listactivity'=>$listactivity]);
    }
    public function UploadDestination(Request $request){
        $all = $request->all();

        $addsummary = new ActivitySummary();
        $addsummary->activitytitle = $request['activitytitle'];
        $addsummary->activitysummary = $request['activity_summary'];

        $image = $request->file('activityimage');
        $addsummary->activityimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/ActivityCoverImage');
        $image->move($destinationPath, $imagename);
        $addsummary->save();

        $lastid = $addsummary->id;
        $activity_day = $request['activityday'];

        foreach ($activity_day  as $key => $activity_days){
            $adddetails = new ActivityDetails();
            $adddetails->activityday = $request['activityday'][$key];
            $adddetails->activitylocation = $request['activitylocation'][$key];
            $adddetails->activitydetails = $request['activitydetails'][$key];
            $adddetails->activitysummary_id = $lastid;
            $adddetails->save();
        }
        return back()->with('success','you successfully added a new destination');

    }
    public function EditActivitySummary(Request $request){
        $id = $request['id'];
        $editsummary = ActivitySummary::find($id);

        if($request->file('activityimage')){

            $editsummary->activitytitle = $request['activitytitle'];
            $editsummary->activitysummary = $request['activity_summary'];

            $image = $request->file('activityimage');
            $editsummary->activityimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/ActivityCoverImage');
            $image->move($destinationPath, $imagename);
            $editsummary->save();

            return back()->with('success','you successfully edited destination');
        }else{

            $editsummary->activitytitle = $request['activitytitle'];
            $editsummary->activitysummary = $request['activity_summary'];
            $editsummary->save();
            return back()->with('success','you successfully edited destination');
        }
    }
    public function DeleteActivitySummary(Request $request){
        $id = $request['id'];
        $deletesummary = ActivitySummary::find($id);
        $deletesummary->delete();
        $deleteactivity = ActivityDetails::where('activitysummary_id',$id)->delete();
        return back()->with('success','you successfully deleted destination');
    }
    public function EnquireList(){
        $list = EnquireNow::join('Attractions', 'Attractions.id', '=', 'enquire_now.destination_id')
            ->select('enquire_now.*','Attractions.attraction_name')
            ->get();
//        dd($list);
        return view('backend.EnquireList')->with(['list'=>$list,]);
    }
    public function SetPrice(Request $request){
        $id = $request['id'];
        $getbookinfo = SetBookingPrice::where('booking_id',$id)->get();

        return view('backend.SetPrice')->with(['id'=>$id,'getbookinfo'=>$getbookinfo]);
    }
    public function AddBookingPrice(Request $request){
        $all = $request->all();
        $add = new SetBookingPrice();
        $add->booking_id = $request['booking_id'];
        $add->booking_price = $request['booking_price'];
        $add->booking_moreinfo = $request['booking_moreinfo'];

        $image = $request->file('booking_invoice');
        $add->booking_invoice = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/BookingInvoices');
        $image->move($destinationPath, $imagename);
        $add->save();

        return back()->with('success','you successfully added prices info');
    }
    public function UpdateBookingPrice(Request $request){
        $all = $request->all();
        if($request->file('booking_invoice')){

            $id = $request['id'];
            $update = SetBookingPrice::find($id);
            $update->booking_id = $request['booking_id'];
            $update->booking_price = $request['booking_price'];
            $update->booking_moreinfo = $request['booking_moreinfo'];

            $image = $request->file('booking_invoice');
            $update->booking_invoice = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/BookingInvoices');
            $image->move($destinationPath, $imagename);
            $update->save();
            return back()->with('success','you successfully updated prices info');

        }else{
            $id = $request['id'];
            $update = SetBookingPrice::find($id);
            $update->booking_id = $request['booking_id'];
            $update->booking_price = $request['booking_price'];
            $update->booking_moreinfo = $request['booking_moreinfo'];
            $update->save();
            return back()->with('success','you successfully updated prices info');
        }
    }
    public function EnquiryEmail(Request $request) {
//        $id = '1';
        $id = $request['id'];
        $getinfo = SetBookingPrice::where('booking_id',$id)->join('enquire_now', 'enquire_now.id', '=', 'set_booking_prices.booking_id')
            ->select('enquire_now.destination_id','enquire_now.enquire_title','enquire_now.enquire_first_name',
                'enquire_now.enquire_title','enquire_now.enquire_title','enquire_now.enquire_title',
                'enquire_now.enquire_last_time','enquire_now.enquire_telephone','enquire_now.enquire_email',
                'enquire_now.enquire_country','set_booking_prices.*')
            ->get();

        foreach ($getinfo as $data){
            $names = $data->enquire_first_name;
            $email = $data->enquire_email;
            $enquire_id = $data->id;
            $booking_price = $data->booking_price;
            $invoicename = $data->booking_invoice;

        }

        if (empty($booking_price)){
            return back()->with('success','set prices and invoice before you send an email');
        }else{
            $message = "Thank you for enquiring Palast Tours destination.";
            $message2 = "Please visit the link below to complete your destination Enquire.";
            $message3 = "Please download the attached invoice.";
            $subjectmail = "Complete Your Enquire";

            $data = array(
                'namemail'=>'Palast Tours',
                'messagemail'=>$message,
            );


            Mail::send('backend.mail',
                array(
                    'namemail'=>'Palast Tours',
                    'name' => $names,
                    'email' => $email,
                    'messagemail' => $message,
                    'message2'=>$message2,
                    'message3'=>$message3,
                    'invoicename'=>$invoicename,
                    'id'=>$id,
                ), function($message) use ($email, $names,$subjectmail)
                {
                    $message->from('kalisa913@gmail.com', 'Palast Tours');
                    $message->to('ccelyse1@gmail.com', 'Palast Tours')->subject($subjectmail);
                });

            return back()->with('success','you successfully sent an email');
        }

//        return view('ThankYou');
    }
}
