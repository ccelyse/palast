<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquireNow extends Model
{
    protected $table = "enquire_now";
    protected $fillable = ['id','destination_id','enquire_title','enquire_first_name','enquire_last_time','enquire_telephone','enquire_email',
    'enquire_country','enquire_subject','enquire_message'];
}
