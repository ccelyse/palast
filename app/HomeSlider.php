<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    protected $table = "homeslider";
    protected $fillable = ['id','slidercaption','sliderimage'];
}
