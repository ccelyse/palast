<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurFleetRow extends Model
{
    protected $table = "Ourfleetrow";
    protected $fillable = ['id','ourfleetcaption','ourfleetmore','ourfleetimage'];
}
